# Go-Script

Go-Script is a system for writing scripts using Go syntax. It's goal is to be as
similar (compiled) Go as is feasible, however there are inevitable limitations.
Interoperation between Go script and compiled Go functions is limited. Only
primitive values (numbers and strings) are fully interoperable.
