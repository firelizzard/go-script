package main

import (
	"errors"
	"fmt"
	"go/ast"
	"go/token"
	"io"
	"log"
	"os"
	"strings"

	"github.com/ergochat/readline"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/firelizzard/go-script/pkg/script"
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/inject"
	"gitlab.com/firelizzard/go-script/pkg/vm/parse"
	"gitlab.com/firelizzard/go-script/pkg/vm/program"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func main() { _ = cmd.Execute() }

var cmd = &cobra.Command{
	Use:   "repl",
	Short: "Go-script REPL",
	Args:  cobra.NoArgs,
	Run:   run,
}

var stdoutColor = color.New(color.FgHiBlack)
var stderrColor = color.New(color.BgRed)

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%+v", err)
	}
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %+v", append(otherArgs, err)...)
	}
}

func run(*cobra.Command, []string) {
	r := &repl{world: program.NewWorld()}
	_, err := r.Execute(
		values.String{V: "fmt"},
		&execute.OpImport{},
	)
	check(err)

	r.Run()
}

type repl struct {
	world *program.World
	decls map[string]vm.Declaration

	fset      *token.FileSet
	unmatched []token.Token
	src       string
	count     int
}

func (r *repl) Declare(_ vm.Context, decl vm.Declaration) error { return vm.ErrSealed }

func (r *repl) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if d, ok := r.decls[name]; ok {
		return d, nil
	}
	return r.world.Resolve(ctx, name)
}

func (r *repl) Run() {
	const prompt = "> "
	rl, err := readline.New(prompt)
	check(err)
	defer rl.Close()

	// Redraw the prompt correctly after log output
	log.SetOutput(rl.Stderr())

	for {
		if len(r.unmatched) == 0 {
			rl.SetPrompt(prompt)
		} else {
			rl.SetPrompt("| " + strings.Repeat("    ", len(r.unmatched)))
		}

		line, err := rl.ReadLine()
		switch {
		case err == nil:
			// Ok
			r.src += line

		case errors.Is(err, io.EOF):
			// Closing
			return

		case errors.Is(err, readline.ErrInterrupt):
			// Reset
			r.src, r.unmatched = "", nil
			continue

		default:
			// Unknown error
			check(err)
		}

		err = r.eval(r.src, rl)
		var err2 *script.ErrIncomplete
		switch {
		case err == nil:
			r.src, r.unmatched = "", nil

		case errors.As(err, &err2):
			r.unmatched = err2.Stack

		default:
			r.src, r.unmatched = "", nil
			color.HiRed("%v", err)
		}
	}
}

func (r *repl) eval(src string, rl *readline.Instance) error {
	if r.fset == nil {
		r.fset = token.NewFileSet()
	}

	r.count++
	fname := fmt.Sprintf("%d.go", r.count)
	f, err := script.Read(r.fset, fname, src)
	if err != nil {
		return err
	}

	scope := &tempScope{parent: r}
	ctx := r.world.NewContext()
	ctx.Scope().Push(scope)

	ctx = inject.CaptureStdout(ctx, &colorWriter{stdoutColor, rl.Stdout()})
	ctx = inject.CaptureStderr(ctx, &colorWriter{stderrColor, rl.Stderr()})

	if f.Name.Name != "main" {
		pkg := r.world.LoadOrCreatePackage(f.Name.Name, "./"+f.Name.Name)
		err = pkg.NewFile(fname).ParseAST(f)
		return err
	}

	var prog execute.Program
	for _, decl := range f.Decls {
		var ops execute.Program
		var err error

		// If the decl is `func main() { ... }`, execute the contents directly
		fn, ok := decl.(*ast.FuncDecl)
		if ok && fn.Name.Name == "main" {
			ops, err = parse.Parse(parse.Stmt, fn.Body.List...)
		} else {
			ops, err = parse.Parse(parse.Decl, decl)
		}
		if err != nil {
			return err
		}

		prog = append(prog, ops...)
	}

	err = prog.Execute(ctx)
	if err != nil {
		return err
	}

	// Update the REPL definitions, overwriting any previous values
	if r.decls == nil {
		r.decls = scope.decls
	} else {
		for name, decl := range scope.decls {
			r.decls[name] = decl
		}
	}

	result := ctx.Stack().PopI(0)
	if len(result) == 0 {
		return nil
	}

	var out []string
	err = execute.Program{
		execute.Program(result).AsLiteral(),
		inject.Value(func(args ...any) {
			for _, arg := range args {
				out = append(out, fmt.Sprint(arg))
			}
		}),
		&values.OpCall{},
	}.Execute(ctx)
	if err != nil {
		return err
	}

	fmt.Fprintln(rl.Stdout(), strings.Join(out, ", "))
	return nil
}

func (r *repl) Execute(values ...vm.Value) ([]vm.Value, error) {
	scope := &tempScope{parent: r}
	ctx := r.world.NewContext()
	ctx.Scope().Push(scope)
	err := execute.Program(values).Execute(ctx)
	if err != nil {
		return nil, err
	}

	// Update the REPL definitions, overwriting any previous values
	if r.decls == nil {
		r.decls = scope.decls
	} else {
		for name, decl := range scope.decls {
			r.decls[name] = decl
		}
	}

	return ctx.Stack().PopI(0), err
}

type tempScope struct {
	parent vm.Scope
	decls  map[string]vm.Declaration
}

func (s *tempScope) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if d, ok := s.decls[name]; ok {
		return d, nil
	}
	return s.parent.Resolve(ctx, name)
}

func (s *tempScope) Declare(_ vm.Context, decl vm.Declaration) error {
	name := decl.Name()
	if _, ok := s.decls[name]; ok {
		return fmt.Errorf("%v has already been declared", name)
	}
	if s.decls == nil {
		s.decls = map[string]vm.Declaration{}
	}
	s.decls[name] = decl
	return nil
}

type colorWriter struct {
	Color *color.Color
	Out   io.Writer
}

func (c *colorWriter) Write(b []byte) (int, error) {
	return c.Color.Fprintf(c.Out, "%s", b)
}
