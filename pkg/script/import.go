package script

import (
	"errors"
	"fmt"
	"go/ast"
	"go/token"
	"strconv"

	"golang.org/x/tools/go/ast/astutil"
)

const importPlaceholderFunc = "__import__"

func ResolveImports[N ast.Node](n N, resolve func(path string) (ast.Node, error)) (N, error) {
	var theErr error
	m := astutil.Apply(n, func(c *astutil.Cursor) bool {
		if theErr != nil {
			return false
		}

		i, ok := c.Node().(*Import)
		if !ok {
			return true
		}

		r, err := resolve(i.path)
		if err != nil {
			theErr = err
			return false
		}

		switch r := r.(type) {
		case *ast.BlockStmt:
			c.Delete()
			for _, stmt := range r.List {
				c.InsertBefore(stmt)
			}
			return false

		case ast.Stmt:
			c.Delete()
			c.InsertBefore(r)
			return false

		case *ast.File:
			var hasMain, hasOthers bool
			var stmts []ast.Stmt
			for _, decl := range r.Decls {
				switch decl := decl.(type) {
				case *ast.FuncDecl:
					if decl.Name.Name != "main" {
						theErr = errors.New("cannot include a script containing function declarations (other than main)")
						return false
					}
					hasMain = true
					stmts = append(stmts, decl.Body.List...)
					continue

				case *ast.GenDecl:
					if len(decl.Specs) == 0 {
						continue
					}
					if decl.Tok == token.IMPORT {
						theErr = errors.New("cannot include a script containing imports")
						return false
					}
				}
				hasOthers = true
				stmts = append(stmts, &ast.DeclStmt{
					Decl: decl,
				})
			}
			if hasMain && hasOthers {
				theErr = errors.New("cannot include a script containing func main plus other declarations")
				return false
			}

			c.Delete()
			for _, stmt := range stmts {
				c.InsertBefore(stmt)
			}
			return false

		default:
			theErr = fmt.Errorf("cannot import %T", r)
			return false
		}

	}, nil)
	return m.(N), theErr
}

type Import struct {
	ast.ExprStmt
	path string
}

func import2custom(c *astutil.Cursor) bool {
	expr, ok := c.Node().(*ast.ExprStmt)
	if !ok {
		return true
	}

	call, ok := expr.X.(*ast.CallExpr)
	if !ok || len(call.Args) != 1 {
		return true
	}

	fn, ok := call.Fun.(*ast.Ident)
	if !ok || fn.Name != importPlaceholderFunc {
		return true
	}

	arg, ok := call.Args[0].(*ast.BasicLit)
	if !ok || arg.Kind != token.STRING {
		return true
	}

	path, err := strconv.Unquote(arg.Value)
	if err != nil {
		return true
	}

	c.Replace(&Import{
		ExprStmt: *expr,
		path:     path,
	})

	return true
}
