package script

import (
	"go/ast"
	"go/printer"
	"go/token"
	"regexp"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRead(t *testing.T) {
	fset := token.NewFileSet()
	_, err := Read(fset, "", `
		package main

		func main() {
			println("Hello, world")
		}
	`)
	require.NoError(t, err)

	_, err = Read(fset, "", `
		func main() {
			println("Hello, world")
		}
	`)
	require.NoError(t, err)

	_, err = Read(fset, "", `
		println("Hello, world")
	`)
	require.NoError(t, err)

	_, err = Read(fset, "", `
		var msg = "Hello, world"

		println(msg)
	`)
	require.NoError(t, err)
}

func TestImport(t *testing.T) {
	fset := token.NewFileSet()
	f, err := Read(fset, "", `
		import "./messages.go"

		println(msg)
	`)
	require.NoError(t, err)

	f, err = ResolveImports(f, func(path string) (ast.Node, error) {
		require.Equal(t, "./messages.go", path)

		f, err := Read(fset, "messages.go", `var msg = "Hello, world"`)
		require.NoError(t, err)
		return f, nil
	})
	require.NoError(t, err)

	buf := new(strings.Builder)
	err = printer.Fprint(buf, fset, f)
	require.NoError(t, err)
	s := buf.String()
	s = regexp.MustCompile("\n").ReplaceAllString(s, ";")
	s = regexp.MustCompile(";+").ReplaceAllString(s, "; ")
	s = regexp.MustCompile("{;").ReplaceAllString(s, "{")
	s = regexp.MustCompile(`\s+`).ReplaceAllString(s, " ")
	s = regexp.MustCompile("; }").ReplaceAllString(s, " }")
	s = regexp.MustCompile("};").ReplaceAllString(s, "}")
	s = strings.TrimSpace(s)
	require.Equal(t, `package main; func main() { var msg = "Hello, world"; println(msg) }`, s)
}
