package script

import (
	"errors"
	"fmt"
	"go/ast"
	"go/parser"
	"go/scanner"
	"go/token"
	"path"
	"strconv"
	"strings"

	"golang.org/x/tools/go/ast/astutil"
)

func Read(fset *token.FileSet, filename, src string) (*ast.File, error) {
	lc := fmt.Sprintf("/*line %s:1:1*/", filename)

	var tryDecl bool
	var f *ast.File
	var err error
	switch firstToken(src) {
	case token.PACKAGE:
		// Parse as a package
		f, err = parseFile(fset, filename, src)

	case token.FUNC:
		// First token is func, must parse as a package
		f, err = parseFile(fset, filename, "package main; "+lc+src)

	case token.IMPORT, token.CONST, token.TYPE, token.VAR:
		// Try parsing assuming src only has top-level statements
		tryDecl = true
		f, err = parseFile(fset, filename, "package main; "+lc+src)

	default:
		// If the first token is not a valid top-level statement, wrap it with a
		// function
		f, err = parseFile(fset, filename, "package main; func main() {"+lc+src+"\n}")
	}

	if err != nil && tryDecl && errors.As(err, new(scanner.ErrorList)) {
		// Try again wrapped with a function
		f, err = parseFile(fset, filename, "package main; func main() {"+lc+src+"\n}")
	}

	if err != nil {
		return nil, err
	}

	f = astutil.Apply(f, import2custom, nil).(*ast.File)

	return f, nil
}

func firstToken(src string) token.Token {
	var s scanner.Scanner
	fset := token.NewFileSet()
	file := fset.AddFile("", fset.Base(), len(src))
	s.Init(file, []byte(src), nil, 0)
	_, tok, _ := s.Scan()
	return tok
}

var r2l = map[token.Token]token.Token{
	token.RPAREN: token.LPAREN,
	token.RBRACK: token.LBRACK,
	token.RBRACE: token.LBRACE,
}

func parseFile(fset *token.FileSet, filename, src string) (_ *ast.File, err error) {
	var s scanner.Scanner
	file := token.NewFileSet().AddFile("", -1, len(src))
	s.Init(file, []byte(src), nil, 0)

	// If parsing fails, remove the file from the fileset
	defer func() {
		if err != nil {
			fset.RemoveFile(file)
		}
	}()

	var newSrc string
	var lastPos int
	var stack []token.Token
	var mismatched bool
scan:
	for {
		pos, tok, _ := s.Scan()
		switch tok {
		case token.EOF:
			break scan

		case token.IMPORT:
			pos2, tok2, lit2 := s.Scan()
			if tok2 != token.STRING {
				break
			}
			s, err := strconv.Unquote(lit2)
			if err != nil {
				break
			}
			if !(strings.HasPrefix(s, "./") || strings.HasPrefix(s, "../")) || path.Ext(s) == "" {
				break
			}

			newSrc += src[lastPos : pos-1]
			newSrc += importPlaceholderFunc + `(` + lit2 + `)`
			lastPos = int(pos2) + len(lit2) - 1
			continue
		}

		if mismatched {
			continue
		}

		switch tok {
		case token.LPAREN, token.LBRACK, token.LBRACE:
			stack = append(stack, tok)
		case token.RPAREN, token.RBRACK, token.RBRACE:
			expect, n := r2l[tok], len(stack)
			if n == 0 || stack[n-1] != expect {
				mismatched = true
			} else {
				stack = stack[:n-1]
			}
		}
	}
	newSrc += src[lastPos:]

	if !mismatched && len(stack) > 0 {
		return nil, &ErrIncomplete{Stack: stack}
	}

	return parser.ParseFile(fset, filename, newSrc, 0)
}

type ErrIncomplete struct {
	Stack []token.Token
}

func (e *ErrIncomplete) Error() string {
	if len(e.Stack) == 0 {
		return "unmatched ???"
	}
	return fmt.Sprintf("unmatched %q", e.Stack[len(e.Stack)-1])
}
