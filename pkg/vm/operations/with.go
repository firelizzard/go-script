package operations

import "gitlab.com/firelizzard/go-script/pkg/vm"

type EqualValue interface {
	// Equal checks the equality of two values.
	Equal(vm.Value) (bool, error)
	vm.Value
}

type AddValue interface {
	// Add returns the sum of two values.
	Add(vm.Value) (vm.Value, error)
	vm.Value
}

type SubValue interface {
	// Sub returns the difference of two values.
	Sub(vm.Value) (vm.Value, error)
	vm.Value
}

type MulValue interface {
	// Mul returns the product of two values.
	Mul(vm.Value) (vm.Value, error)
	vm.Value
}

type DivValue interface {
	// Div returns the quotient of two values.
	Div(vm.Value) (vm.Value, error)
	vm.Value
}

type LessThanValue interface {
	LessThan(vm.Value) (bool, error)
	vm.Value
}

type GreaterThanValue interface {
	GreaterThan(vm.Value) (bool, error)
	vm.Value
}

type LenValue interface {
	// Len returns the length of the value.
	Len() int
	vm.Value
}

type IndexValue interface {
	// Index returns the element at the specified index.
	Index(vm.Value) (vm.Value, error)
	vm.Value
}

type SliceValue interface {
	// Slice returns a slice of the value.
	Slice(a, b, c vm.Value) (vm.Value, error)
	vm.Value
}

type NotValue interface {
	// Not returns the logical inverse of a boolean value (e.g. false -> true).
	Not() vm.Value
	vm.Value
}

type NegValue interface {
	// Neg returns the arithmetic inverse of a numeric value (e.g. -2 -> +2).
	Neg() vm.Value
	vm.Value
}

type InvValue interface {
	// Inv requires the bitwise inverse of a binary value (e.g. 0xA5 -> 0x59)
	Inv() vm.Value
	vm.Value
}
