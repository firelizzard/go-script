package operations

import (
	"errors"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type Slice struct {
	A, B, C bool
}

var _ vm.Op = (*Slice)(nil)

func (o *Slice) Type() vm.Type { return common.TypeFor(o) }

func (o *Slice) Execute(ctx vm.Context) error {
	v, err := common.PopUnwrap[SliceValue](ctx)
	if err != nil {
		return err
	}

	var a, b, c vm.Value
	var e1, e2, e3 error
	if o.A {
		a, e1 = common.PopUnwrap[vm.Value](ctx)
	}
	if o.B {
		b, e2 = common.PopUnwrap[vm.Value](ctx)
	}
	if o.C {
		c, e3 = common.PopUnwrap[vm.Value](ctx)
	}
	if err := errors.Join(e1, e2, e3); err != nil {
		return err
	}

	u, err := v.Slice(a, b, c)
	if err != nil {
		return err
	}

	ctx.Stack().Push(u)
	return nil
}
