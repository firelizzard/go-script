package operations

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type BinaryExpr interface {
	vm.Op
	Apply(ctx vm.Context, v, u vm.Value) (vm.Value, error)

	Named(string) BinaryExpr
	SameIn() BinaryExpr
	SameOut() BinaryExpr
	ElemOut() BinaryExpr
}

func Binary[V any](fn func(V, vm.Value) (vm.Value, error)) BinaryExpr {
	return &BinaryExprFor[V]{Func: fn}
}

type BinaryExprFor[V any] struct {
	Name string
	Func func(V, vm.Value) (vm.Value, error)
	In   CoercionType
	Out  CoercionType
}

var _ vm.Value = (*BinaryExprFor[any])(nil)

func (b *BinaryExprFor[V]) Type() vm.Type { return common.TypeFor(b) }

func (b *BinaryExprFor[V]) Named(name string) BinaryExpr {
	b.Name = name
	return b
}

func (b *BinaryExprFor[V]) SameIn() BinaryExpr {
	b.In = CoerceToSame
	return b
}

func (b *BinaryExprFor[V]) SameOut() BinaryExpr {
	b.Out = CoerceToSame
	return b
}

func (b *BinaryExprFor[V]) ElemOut() BinaryExpr {
	b.Out = CoerceToElem
	return b
}

func (o *BinaryExprFor[V]) Execute(ctx vm.Context) error {
	// Pop the operands
	u, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return err
	}
	v, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return err
	}

	w, err := o.Apply(ctx, v, u)
	if err != nil {
		return err
	}
	ctx.Stack().Push(w)
	return nil
}

func (o *BinaryExprFor[V]) Apply(ctx vm.Context, v, u vm.Value) (vm.Value, error) {
	// Pick the type to use for the conversion
	typ, err := o.In.FindCommonType(ctx, v.Type(), u.Type())
	if err != nil {
		return nil, err
	}

	// Coerce V and U to T
	v, err = o.In.Coerce(ctx, v, typ, true)
	if err != nil {
		return nil, err
	}
	u, err = o.In.Coerce(ctx, u, typ, true)
	if err != nil {
		return nil, err
	}

	// Get the underlying value(s)
	v = common.Underlying(v)
	if o.In == CoerceToSame {
		u = common.Underlying(u)
	}

	// Execute the operation
	if v, ok := v.(V); !ok {
		return nil, common.OperatorNotDefined(o.Name, typ)
	} else {
		u, err = o.Func(v, u)
	}
	if err != nil {
		return nil, err
	}

	// Coerce the output
	return o.Out.Coerce(ctx, u, typ, true)
}
