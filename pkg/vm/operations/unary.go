package operations

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type UnaryExpr interface {
	vm.Op
	Named(string) UnaryExpr
	SameOut() UnaryExpr
	ElemOut() UnaryExpr
}

func Unary[V any](fn func(V) (vm.Value, error)) UnaryExpr {
	return &UnaryExprFor[V]{Func: fn}
}

type UnaryExprFor[V any] struct {
	Name string
	Func func(V) (vm.Value, error)
	Out  CoercionType
}

var _ vm.Value = (*UnaryExprFor[any])(nil)

func (b *UnaryExprFor[V]) Type() vm.Type { return common.TypeFor(b) }

func (b *UnaryExprFor[V]) Named(name string) UnaryExpr {
	b.Name = name
	return b
}

func (b *UnaryExprFor[V]) SameOut() UnaryExpr {
	b.Out = CoerceToSame
	return b
}

func (b *UnaryExprFor[V]) ElemOut() UnaryExpr {
	b.Out = CoerceToElem
	return b
}

func (o *UnaryExprFor[V]) Call(ctx vm.Context, in []vm.Value) ([]vm.Value, error) {
	if len(in) != 1 {
		return nil, fmt.Errorf("invalid number of arguments: want 1, got %d", len(in))
	}

	v, err := common.Unwrap[vm.Value](ctx, in[0])
	if err != nil {
		return nil, err
	}

	// Ensure V supports the operation
	_, ok := common.Underlying(v).(V)
	if !ok {
		return nil, common.OperatorNotDefined(o.Name, v.Type())
	}

	// Execute
	w, err := o.execute(ctx, v)
	if err != nil {
		return nil, err
	}

	return []vm.Value{w}, nil
}

func (o *UnaryExprFor[V]) Execute(ctx vm.Context) error {
	// Pop the operands
	v, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return err
	}

	// Ensure V supports the operation
	_, ok := common.Underlying(v).(V)
	if !ok {
		return common.OperatorNotDefined(o.Name, v.Type())
	}

	// Execute
	w, err := o.execute(ctx, v)
	if err != nil {
		return err
	}

	ctx.Stack().Push(w)
	return nil
}

func (o *UnaryExprFor[V]) execute(ctx vm.Context, v vm.Value) (vm.Value, error) {
	// Is typeof(V) a named type?
	if v, ok := v.(vm.Named); ok {
		// Execute with the underlying value of V
		w, err := o.execute(ctx, v.Underlying())
		if err != nil {
			return nil, err
		}

		return o.Out.Coerce(ctx, w, v.Type(), true)
	}

	// Execute
	w, err := o.Func(v.(V))
	return w, err
}
