package operations

import (
	"fmt"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type CoercionType int

const (
	// NoCoercion does not coerce the result of the expression.
	NoCoercion CoercionType = iota

	// CoerceToSame coerces the result of the expression to match that of the operand(s).
	CoerceToSame

	// CoerceToElem coerces the result of the expression to match that of the
	// operand's element.
	CoerceToElem
)

func (rt CoercionType) Coerce(ctx vm.Context, v vm.Value, t vm.Type, force bool) (vm.Value, error) {
	switch rt {
	case CoerceToSame:
		return t.Convert(ctx, v, force)

	case CoerceToElem:
		if et, ok := t.(vm.ElemType); !ok {
			return nil, fmt.Errorf("invalid type: want %v, got %T", reflect.TypeFor[vm.ElemType](), t)
		} else {
			t = et.Elem()
		}
		return t.Convert(ctx, v, force)

	default:
		return v, nil
	}
}

func (rt CoercionType) FindCommonType(ctx vm.Context, r, s vm.Type) (vm.Type, error) {
	switch rt {
	case CoerceToSame:
		// Ok

	case CoerceToElem:
		if et, ok := r.(vm.ElemType); !ok {
			return nil, fmt.Errorf("invalid type: want %v, got %T", reflect.TypeFor[vm.ElemType](), r)
		} else {
			r = et.Elem()
		}

	default:
		return r, nil
	}

	ok, err := s.AssignableTo(ctx, r)
	if err != nil {
		return nil, err
	} else if ok {
		return r, nil
	}

	ok, err = r.AssignableTo(ctx, s)
	if err != nil {
		return nil, err
	} else if ok {
		return s, nil
	}

	return nil, common.CannotConvert(s, r)
}
