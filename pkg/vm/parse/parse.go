package parse

import (
	"go/ast"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
)

func Parse[V any](factory factory[V], src ...V) (execute.Program, error) {
	ops, errs := all(factory, src)(nil, nil)
	if !errs.ok() {
		return nil, errs.join()
	}
	return ops.Join(), nil
}

type parser func(ops *Ops, errs *Errors) (*Ops, *Errors)
type factory[V any] func(src V) parser

func (p parser) debug(yes bool) parser {
	if !yes {
		return p
	}
	return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
		return p(ops, errs)
	}
}

func (p parser) validate(fn func(ops *Ops, errs *Errors) *Errors) parser {
	return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
		ops, errs = p(ops, errs)
		return ops, fn(ops, errs)
	}
}

func (p parser) asBlock() parser {
	return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
		i := ops.Len()
		ops, errs = p(ops, errs)
		prog := execute.Program(ops.PopI(i)).AsLiteral()
		return ops.Push(prog), errs
	}
}

func noop(ops *Ops, errs *Errors) (*Ops, *Errors) {
	return ops, errs
}

func errorf(node ast.Node, format string, args ...any) parser {
	return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
		return ops, errs.addf(node, format, args...)
	}
}

func pushVal(val vm.Value) parser {
	return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
		return ops.Push(val), errs
	}
}

func pushOp(op vm.Op) parser {
	return pushVal(op)
}

func join(p ...parser) parser {
	return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
		for _, p := range p {
			ops, errs = p(ops, errs)
		}
		return ops, errs
	}
}

func all[V any](factory factory[V], src []V) parser {
	var p []parser
	for _, src := range src {
		p = append(p, factory(src))
	}
	return join(p...)
}

func validate[V any](factory factory[V], validate func(src V, ops *Ops, errs *Errors) *Errors) factory[V] {
	return func(src V) parser {
		parser := factory(src)
		return func(ops *Ops, errs *Errors) (*Ops, *Errors) {
			ops, errs = parser(ops, errs)
			return ops, validate(src, ops, errs)
		}
	}
}

func when(cond bool, then parser) parser {
	if cond {
		return then
	} else {
	}
	return noop
}

func whenElse(cond bool, then, else_ parser) parser {
	if cond {
		return then
	}
	return else_
}

func maybe[V comparable](factory factory[V], src V) parser {
	var z V
	if src == z {
		return noop
	}
	return factory(src)
}
