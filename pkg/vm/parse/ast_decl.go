package parse

import (
	"go/ast"
	"go/token"

	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func Decl(src ast.Decl) parser {
	switch src := src.(type) {
	case *ast.GenDecl:
		return genDecl(src)
	case *ast.FuncDecl:
		return funcDecl(src)

	case *ast.BadDecl:
		return errorf(src, "bad declaration")
	default:
		return errorf(src, "unsupported declaration %T", src)
	}
}

func genDecl(src *ast.GenDecl) parser {
	var p []parser
	for _, spec := range src.Specs {
		val, ok := spec.(*ast.ValueSpec)
		if !ok || src.Tok != token.CONST {
			p = append(p, Spec(spec))
			continue
		}

		// Constants need special processing
		//
		// TODO: iota/etc, see go/types/decl.go
		p = append(p, join(
			all(Expr, val.Values).asBlock(),
			all(ident, val.Names).asBlock(),
			maybe(Expr, val.Type).asBlock(),
			pushOp(&values.OpMakeVariable{IsConst: true}),
			pushOp(&execute.Declare{Count: len(val.Names)})))
	}
	return join(p...)
}

func funcDecl(src *ast.FuncDecl) parser {
	if src.Type.TypeParams != nil && len(src.Type.TypeParams.List) > 0 {
		return errorf(src, "generic functions are not supported")
	}
	if src.Body == nil {
		return errorf(src, "missing function body")
	}

	return join(
		Stmt(src.Body).asBlock(),
		funcType2(src.Type, src.Recv),
		ident(src.Name),
		pushOp(&values.OpMakeFuncDecl{}),
		maybe(recvType, src.Recv),
		whenElse(src.Recv == nil,
			pushOp(&execute.Declare{}),
			pushOp(&values.OpDeclareMember{})))
}

func recvType(src *ast.FieldList) parser {
	if src == nil {
		return nil
	}
	if len(src.List) != 1 && len(src.List[0].Names) > 1 {
		return errorf(src, "invalid method receiver(s)")
	}

	var ptr int
	for recv := src.List[0].Type; ; {
		if ptr > 1 {
			return errorf(src, "method receiver cannot be a pointer pointer")
		}
		switch r := recv.(type) {
		case *ast.Ident:
			return ident(r)
		case *ast.StarExpr:
			recv = r.X
			ptr++
		default:
			return errorf(src, "%T is not a valid method receiver", recv)
		}
	}
}
