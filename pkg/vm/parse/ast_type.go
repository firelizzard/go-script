package parse

import (
	"go/ast"

	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func arrayType(src *ast.ArrayType) parser {
	return join(
		maybe(Expr, src.Len),
		Expr(src.Elt),
		pushOp(&values.OpMakeArrayType{Slice: src.Len == nil}))
}

func chanType(src *ast.ChanType) parser {
	return errorf(src, "unsupported expression *ast.ChanType")
}

func funcType(src *ast.FuncType) parser {
	return funcType2(src, nil)
}

func funcType2(src *ast.FuncType, recv *ast.FieldList) parser {
	if src.TypeParams != nil && len(src.TypeParams.List) > 0 {
		return errorf(src, "generic functions are not supported")
	}

	return join(
		maybe(fieldList, recv),
		maybe(fieldList, src.Params),
		maybe(fieldList, src.Results),
		pushOp(&values.OpMakeFuncSig{
			In:  countFields(src.Params, recv),
			Out: countFields(src.Results),
		}))
}

func interfaceType(src *ast.InterfaceType) parser {
	return errorf(src, "unsupported expression *ast.InterfaceType")
}

func mapType(src *ast.MapType) parser {
	return errorf(src, "unsupported expression *ast.MapType")
}

func structType(src *ast.StructType) parser {
	return join(
		fieldList(src.Fields),
		pushOp(&values.OpMakeStructType{
			Fields: countFields(src.Fields),
		}))
}

func fieldList(src *ast.FieldList) parser {
	return all(field, src.List)
}

func field(src *ast.Field) parser {
	return join(
		Expr(src.Type),
		maybe(basicLit, src.Tag),
		all(ident, src.Names).asBlock(),
		pushOp(&values.OpMakeFieldType{
			Tagged: src.Tag != nil,
		}))
}

func countFields(src ...*ast.FieldList) int {
	var count int
	for _, src := range src {
		if src == nil {
			continue
		}
		for _, src := range src.List {
			if len(src.Names) == 0 {
				count++
			} else {
				count += len(src.Names)
			}
		}
	}
	return count
}
