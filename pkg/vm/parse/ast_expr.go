package parse

import (
	"go/ast"
	"go/token"
	"math/big"
	"strconv"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/operations"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func Expr(src ast.Expr) parser {
	switch src := src.(type) {
	case *ast.BasicLit:
		return basicLit(src)
	case *ast.BinaryExpr:
		return binaryExpr(src)
	case *ast.CallExpr:
		return callExpr(src)
	case *ast.CompositeLit:
		return compositeLit(src)
	case *ast.Ellipsis:
		return ellipsis(src)
	case *ast.FuncLit:
		return funcLit(src)
	case *ast.Ident:
		return ident(src)
	case *ast.IndexExpr:
		return indexExpr(src)
	case *ast.IndexListExpr:
		return indexListExpr(src)
	case *ast.KeyValueExpr:
		return keyValueExpr(src)
	case *ast.ParenExpr:
		return parenExpr(src)
	case *ast.SelectorExpr:
		return selectorExpr(src)
	case *ast.SliceExpr:
		return sliceExpr(src)
	case *ast.StarExpr:
		return starExpr(src)
	case *ast.TypeAssertExpr:
		return typeAssertExpr(src)
	case *ast.UnaryExpr:
		return unaryExpr(src)

	case *ast.ArrayType:
		return arrayType(src)
	case *ast.ChanType:
		return chanType(src)
	case *ast.FuncType:
		return funcType(src)
	case *ast.InterfaceType:
		return interfaceType(src)
	case *ast.MapType:
		return mapType(src)
	case *ast.StructType:
		return structType(src)

	case *ast.BadExpr:
		return errorf(src, "bad expression")
	default:
		return errorf(src, "unsupported expression %T", src)
	}
}

func basicLit(src *ast.BasicLit) parser {
	var value vm.Value
	var err error
	switch src.Kind {
	case token.INT:
		var v big.Int
		err = v.UnmarshalText([]byte(src.Value))
		value = &values.UntypedInt{V: v}

	case token.FLOAT:
		var v big.Float
		err = v.UnmarshalText([]byte(src.Value))
		value = &values.UntypedFloat{V: v}

	case token.STRING:
		var v string
		v, err = strconv.Unquote(src.Value)
		value = values.UntypedString{V: v}

	default:
		return errorf(src, "unsupported basic literal %v", src.Kind)
	}

	if err != nil {
		return errorf(src, "invalid %v literal: %w", src.Kind, err)
	}
	return pushVal(value)
}

func binaryExpr(src *ast.BinaryExpr) parser {
	var op operations.BinaryExpr
	switch src.Op {
	case token.EQL:
		op = operations.Binary(op2Bool(operations.EqualValue.Equal))
	case token.NEQ:
		op = operations.Binary(op2BoolInv(operations.EqualValue.Equal))
	case token.ADD:
		op = operations.Binary(operations.AddValue.Add).SameOut()
	case token.SUB:
		op = operations.Binary(operations.SubValue.Sub).SameOut()
	case token.MUL:
		op = operations.Binary(operations.MulValue.Mul).SameOut()
	case token.QUO:
		op = operations.Binary(operations.DivValue.Div).SameOut()
	case token.LSS:
		op = operations.Binary(op2Bool(operations.LessThanValue.LessThan))
	case token.GTR:
		op = operations.Binary(op2Bool(operations.GreaterThanValue.GreaterThan))
	case token.LEQ:
		op = operations.Binary(op2BoolInv(operations.GreaterThanValue.GreaterThan))
	case token.GEQ:
		op = operations.Binary(op2BoolInv(operations.LessThanValue.LessThan))
	default:
		return errorf(src, "unsupported binary expression %v", src.Op)
	}

	return join(
		Expr(src.X),
		Expr(src.Y),
		pushOp(op.SameIn().Named(src.Op.String())))
}

func callExpr(src *ast.CallExpr) parser {
	return join(
		all(Expr, src.Args).asBlock(),
		Expr(src.Fun),
		pushOp(&values.OpCall{}))
}

func compositeLit(src *ast.CompositeLit) parser {
	return join(
		Expr(src.Type),
		all(Expr, src.Elts),
		pushOp(&values.OpMakeComposite{NumArgs: len(src.Elts)}))
}

func ellipsis(src *ast.Ellipsis) parser {
	return errorf(src, "unsupported expression *ast.Ellipsis")
}

func funcLit(src *ast.FuncLit) parser {
	return join(
		Stmt(src.Body).asBlock(),
		funcType(src.Type),
		pushOp(&values.OpMakeFuncLit{}))
}

func ident(src *ast.Ident) parser {
	return pushVal(values.Identifier{V: src.Name})
}

func indexExpr(src *ast.IndexExpr) parser {
	return join(
		Expr(src.X),
		Expr(src.Index),
		pushOp(operations.Binary(operations.IndexValue.Index).Named("[]").ElemOut()))
}

func indexListExpr(src *ast.IndexListExpr) parser {
	return errorf(src, "unsupported expression *ast.IndexListExpr")
}

func keyValueExpr(src *ast.KeyValueExpr) parser {
	return errorf(src, "unsupported expression *ast.KeyValueExpr")
}

func parenExpr(src *ast.ParenExpr) parser {
	// I think it should be safe to just parse the interior expression
	return Expr(src.X)
}

func selectorExpr(src *ast.SelectorExpr) parser {
	return join(
		Expr(src.X),
		ident(src.Sel),
		pushOp(&execute.Select{}))
}

func sliceExpr(src *ast.SliceExpr) parser {
	return join(
		maybe(Expr, src.Max),
		maybe(Expr, src.High),
		maybe(Expr, src.Low),
		Expr(src.X),
		pushOp(&operations.Slice{
			A: src.Low != nil,
			B: src.High != nil,
			C: src.Max != nil,
		}))
}

func starExpr(src *ast.StarExpr) parser {
	return errorf(src, "unsupported expression *ast.StarExpr")
}

func typeAssertExpr(src *ast.TypeAssertExpr) parser {
	return errorf(src, "unsupported expression *ast.TypeAssertExpr")
}

func unaryExpr(src *ast.UnaryExpr) parser {
	var op operations.UnaryExpr
	switch src.Op {
	case token.ADD:
		return Expr(src.X) // Unary add does nothing
	case token.SUB:
		op = operations.Unary(op1NoErr(operations.NegValue.Neg)).SameOut()
	case token.NOT:
		op = operations.Unary(op1NoErr(operations.NotValue.Not)).SameOut()
	case token.XOR:
		op = operations.Unary(op1NoErr(operations.InvValue.Inv)).SameOut()
	// case token.AND:
	// case token.ARROW:
	default:
		return errorf(src, "unsupported unary expression %v", src.Op)
	}
	return join(
		Expr(src.X),
		pushOp(op.Named(src.Op.String())))
}

func op1NoErr[V any](fn func(V) vm.Value) func(V) (vm.Value, error) {
	return func(v V) (vm.Value, error) {
		return fn(v), nil
	}
}

func op2Bool[V any](fn func(V, vm.Value) (bool, error)) func(V, vm.Value) (vm.Value, error) {
	return func(v V, u vm.Value) (vm.Value, error) {
		ok, err := fn(v, u)
		return values.Bool{V: ok}, err
	}
}

func op2BoolInv[V any](fn func(V, vm.Value) (bool, error)) func(V, vm.Value) (vm.Value, error) {
	return func(v V, u vm.Value) (vm.Value, error) {
		ok, err := fn(v, u)
		return values.Bool{V: !ok}, err
	}
}
