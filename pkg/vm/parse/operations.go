package parse

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
)

type Ops []vm.Value

func (s *Ops) Join() execute.Program {
	if s == nil {
		return nil
	}
	return execute.Program(*s)
}

func (s *Ops) Len() int {
	if s == nil {
		return 0
	}
	return len(*s)
}

func (s *Ops) Push(v ...vm.Value) *Ops {
	if s == nil {
		s := Ops(v)
		return &s
	}
	*s = append(*s, v...)
	return s
}

func (s *Ops) Peek() vm.Value {
	if s.Len() == 0 {
		panic("stack underflow")
	}
	return (*s)[len(*s)-1]
}

func (s *Ops) Pop() vm.Value {
	return s.PopN(1)[0]
}

func (s *Ops) PopN(n int) []vm.Value {
	return s.PopI(s.Len() - n)
}

func (s *Ops) PopI(i int) []vm.Value {
	if i < 0 {
		panic("stack underflow")
	}
	if i == 0 && s == nil {
		return nil
	}
	t := make([]vm.Value, s.Len()-i)
	copy(t, (*s)[i:])
	*s = (*s)[:i]
	return t
}
