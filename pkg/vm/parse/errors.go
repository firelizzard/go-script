package parse

import (
	"errors"
	"fmt"
	"go/ast"
)

type Errors []error

func (e *Errors) ok() bool { return e == nil || len(*e) == 0 }

func (e *Errors) add(node ast.Node, err error) *Errors {
	if err == nil {
		return e
	}
	err = &Error{Wrapped: err, Node: node}
	if e == nil {
		return &Errors{err}
	}
	*e = append(*e, err)
	return e
}

func (e *Errors) addf(node ast.Node, format string, args ...any) *Errors {
	return e.add(node, fmt.Errorf(format, args...))
}

func (e *Errors) join() error {
	return errors.Join(*e...)
}

type Error struct {
	Wrapped error
	Node    ast.Node
}

func (e *Error) Error() string {
	return e.Wrapped.Error()
}

func (e *Error) Unwrap() error {
	return e.Wrapped
}
