package parse

import (
	"go/ast"
)

func Node(src ast.Node) parser {
	switch src := src.(type) {
	case ast.Decl:
		return Decl(src)
	case ast.Spec:
		// If a ValueSpec is passed to this function, assume it's `var` not
		// `const`
		return Spec(src)
	case ast.Stmt:
		return Stmt(src)
	case ast.Expr:
		return Expr(src)
	default:
		return errorf(src, "unsupported syntax %T", src)
	}
}
