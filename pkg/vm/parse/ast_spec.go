package parse

import (
	"go/ast"

	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func Spec(src ast.Spec) parser {
	switch src := src.(type) {
	case *ast.ImportSpec:
		return importSpec(src)
	case *ast.ValueSpec:
		return valueSpec(src)
	case *ast.TypeSpec:
		return typeSpec(src)
	default:
		return errorf(src, "unsupported specification %T", src)
	}
}

func importSpec(src *ast.ImportSpec) parser {
	return join(
		basicLit(src.Path),
		maybe(ident, src.Name),
		pushOp(&execute.OpImport{Named: src.Name != nil}),
	)
}

func valueSpec(src *ast.ValueSpec) parser {
	if src.Type == nil && len(src.Values) == 0 {
		return errorf(src, "invalid value spec: must have type and/or value(s)")
	}

	return join(
		all(Expr, src.Values).asBlock(),
		all(ident, src.Names).asBlock(),
		maybe(Expr, src.Type).asBlock(),
		pushOp(&values.OpMakeVariable{}),
		pushOp(&execute.Declare{Count: len(src.Names)}))
}

func typeSpec(src *ast.TypeSpec) parser {
	return join(
		Expr(src.Type),
		when(!src.Assign.IsValid(), join(
			ident(src.Name),
			pushOp(&values.OpMakeNamedType{}),
		)),
		ident(src.Name),
		pushOp(&values.OpMakeTypeDecl{}),
		pushOp(&execute.Declare{}),
	)
}
