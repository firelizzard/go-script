package parse

import (
	"go/ast"
	"go/token"

	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func Stmt(src ast.Stmt) parser {
	switch src := src.(type) {
	case *ast.AssignStmt:
		return assignStmt(src)
	case *ast.BlockStmt:
		return blockStmt(src)
	case *ast.BranchStmt:
		return branchStmt(src)
	case *ast.CaseClause:
		return caseClause(src)
	case *ast.CommClause:
		return commClause(src)
	case *ast.DeclStmt:
		return declStmt(src)
	case *ast.DeferStmt:
		return deferStmt(src)
	case *ast.EmptyStmt:
		return emptyStmt(src)
	case *ast.ExprStmt:
		return exprStmt(src)
	case *ast.ForStmt:
		return forStmt(src)
	case *ast.GoStmt:
		return goStmt(src)
	case *ast.IfStmt:
		return ifStmt(src)
	case *ast.IncDecStmt:
		return incDecStmt(src)
	case *ast.LabeledStmt:
		return labeledStmt(src)
	case *ast.RangeStmt:
		return rangeStmt(src)
	case *ast.ReturnStmt:
		return returnStmt(src)
	case *ast.SelectStmt:
		return selectStmt(src)
	case *ast.SendStmt:
		return sendStmt(src)
	case *ast.SwitchStmt:
		return switchStmt(src)
	case *ast.TypeSwitchStmt:
		return typeSwitchStmt(src)

	case *ast.BadStmt:
		return errorf(src, "bad statement")
	default:
		return errorf(src, "unsupported statement %T", src)
	}
}

func assignStmt(src *ast.AssignStmt) parser {
	if src.Tok != token.DEFINE {
		return join(
			all(Expr, src.Rhs).asBlock(),
			all(Expr, src.Lhs).asBlock(),
			pushOp(&execute.Assign{}))
	}

	lhsExpr := validate(Expr, func(src ast.Expr, ops *Ops, errs *Errors) *Errors {
		if _, ok := ops.Peek().(values.Identifier); !ok {
			errs = errs.addf(src, "cannot use %T as LHS in := assignment", ops.Peek())
		}
		return errs
	})

	// Use an empty Program for the type
	return join(
		all(Expr, src.Rhs).asBlock(),
		all(lhsExpr, src.Lhs).asBlock(),
		pushVal(execute.Program{}.AsLiteral()),
		pushOp(&values.OpMakeVariable{}),
		pushOp(&execute.Declare{Count: len(src.Lhs)}))
}

func blockStmt(src *ast.BlockStmt) parser {
	return join(
		all(Stmt, src.List).asBlock(),
		pushOp(&execute.OpBlock{}))
}

func branchStmt(src *ast.BranchStmt) parser {
	switch src.Tok {
	case token.BREAK:
		return join(
			maybe(ident, src.Label),
			whenElse(src.Label == nil,
				pushOp(&common.OpBreak{}),
				pushOp(&common.OpBreakTo{})))

	case token.CONTINUE:
		return join(
			maybe(ident, src.Label),
			whenElse(src.Label == nil,
				pushOp(&common.OpContinue{}),
				pushOp(&common.OpContinueTo{})))

	default:
		return errorf(src, "unsupported branch type: %v", src.Tok)
	}
}

func caseClause(src *ast.CaseClause) parser {
	return errorf(src, "unsupported statement *ast.CaseClause")
}

func commClause(src *ast.CommClause) parser {
	return errorf(src, "unsupported statement *ast.CommClause")
}

func declStmt(src *ast.DeclStmt) parser {
	return Decl(src.Decl)
}

func deferStmt(src *ast.DeferStmt) parser {
	return errorf(src, "unsupported statement *ast.DeferStmt")
}

func emptyStmt(*ast.EmptyStmt) parser {
	return noop
}

func exprStmt(src *ast.ExprStmt) parser {
	return Expr(src.X)
}

func forStmt(src *ast.ForStmt) parser {
	return join(
		maybe(Stmt, src.Init).asBlock(),
		maybe(Expr, src.Cond).asBlock(),
		Stmt(src.Body).asBlock(),
		maybe(Stmt, src.Post).asBlock(),
		pushOp(&execute.OpFor{}))
}

func goStmt(src *ast.GoStmt) parser {
	return errorf(src, "unsupported statement *ast.GoStmt")
}

func ifStmt(src *ast.IfStmt) parser {
	return join(
		maybe(Stmt, src.Init).asBlock(),
		Expr(src.Cond).asBlock(),
		Stmt(src.Body).asBlock(),
		maybe(Stmt, src.Else).asBlock(),
		pushOp(&execute.OpIf{}))
}

func incDecStmt(src *ast.IncDecStmt) parser {
	return errorf(src, "unsupported statement *ast.IncDecStmt")
}

func labeledStmt(src *ast.LabeledStmt) parser {
	return errorf(src, "unsupported statement *ast.LabeledStmt")
}

func rangeStmt(src *ast.RangeStmt) parser {
	return errorf(src, "unsupported statement *ast.RangeStmt")
}

func returnStmt(src *ast.ReturnStmt) parser {
	return join(
		all(Expr, src.Results).asBlock(),
		pushOp(&common.OpReturn{}),
	)
}

func selectStmt(src *ast.SelectStmt) parser {
	return errorf(src, "unsupported statement *ast.SelectStmt")
}

func sendStmt(src *ast.SendStmt) parser {
	return errorf(src, "unsupported statement *ast.SendStmt")
}

func switchStmt(src *ast.SwitchStmt) parser {
	return errorf(src, "unsupported statement *ast.SwitchStmt")
}

func typeSwitchStmt(src *ast.TypeSwitchStmt) parser {
	return errorf(src, "unsupported statement *ast.TypeSwitchStmt")
}
