package inject

import (
	"fmt"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

var typeContext = reflect.TypeFor[vm.Context]()

func (v goValue) Call(ctx vm.Context, in []vm.Value) ([]vm.Value, error) {
	if v.val.Kind() != reflect.Func {
		return nil, fmt.Errorf("%v is not callable", v.val.Type())
	}

	// If the first input is [vm.Context], inject the context
	if v.val.Type().NumIn() > 0 && typeContext.AssignableTo(v.val.Type().In(0)) {
		in = append([]vm.Value{goValue{reflect.ValueOf(ctx)}}, in...)
	}

	// Validate number of arguments
	typ := v.val.Type()
	err := common.CheckValueCount(len(in), typ.NumIn(), typ.IsVariadic())
	if err != nil {
		return nil, err
	}

	var varArgTyp reflect.Type
	if typ.IsVariadic() {
		varArgTyp = typ.In(typ.NumIn() - 1).Elem()
	}

	// Convert arguments
	goIn := make([]reflect.Value, len(in))
	for i, arg := range in {
		var atyp reflect.Type
		if i < typ.NumIn()-1 || !typ.IsVariadic() {
			atyp = typ.In(i)
		} else {
			atyp = varArgTyp
		}

		arg, err := common.Unwrap[vm.Value](ctx, arg)
		if err != nil {
			return nil, err
		}
		arg, err = goType{typ: atyp}.Convert(ctx, arg, false)
		if err != nil {
			return nil, err
		}
		goIn[i] = asNativeValue(ctx, arg)
	}

	// Call
	goOut := v.val.Call(goIn)

	// Convert return values
	out := make([]vm.Value, len(goOut))
	for i, v := range goOut {
		out[i] = Value(v)
	}
	return out, nil
}
