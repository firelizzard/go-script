package inject

import (
	"io"
	"os"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

var cStdout = capturer[io.Writer]{"__stdout__", os.Stdout}
var cStderr = capturer[io.Writer]{"__stderr__", os.Stderr}

func CaptureStdout(ctx vm.Context, wr io.Writer) vm.Context { return cStdout.capture(ctx, wr) }
func ResolveStdout(ctx vm.Context) io.Writer                { return cStdout.resolve(ctx) }
func CaptureStderr(ctx vm.Context, wr io.Writer) vm.Context { return cStderr.capture(ctx, wr) }
func ResolveStderr(ctx vm.Context) io.Writer                { return cStderr.resolve(ctx) }

type capturer[V any] struct {
	name     string
	fallback V
}

func (c *capturer[V]) capture(ctx vm.Context, value V) vm.Context {
	return &captured[V]{
		Context: ctx,
		name:    c.name,
		value:   value,
	}
}

func (c *capturer[V]) resolve(ctx vm.Context) V {
	decl, err := ctx.World().Resolve(ctx, c.name)
	if err != nil {
		return c.fallback
	}
	if c, ok := decl.(*captureDecl[V]); ok {
		return c.value
	}
	return c.fallback
}

type captured[V any] struct {
	vm.Context
	name  string
	value V
}

func (c *captured[V]) New() vm.Context { return &captured[V]{c.Context.New(), c.name, c.value} }
func (c *captured[V]) World() vm.Scope { return (*captureWorld[V])(c) }

type captureWorld[V any] captured[V]

func (c *captureWorld[V]) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if c.name == name {
		return (*captureDecl[V])(c), nil
	}
	return c.Context.World().Resolve(ctx, name)
}

func (c *captureWorld[V]) Declare(ctx vm.Context, decl vm.Declaration) error {
	// Pass through all declarations to the parent scope
	return c.Context.World().Declare(ctx, decl)
}

type captureDecl[V any] captured[V]

func (c *captureDecl[V]) Type() vm.Type                  { return common.TypeFor(c) }
func (c *captureDecl[V]) Kind() vm.DeclKind              { return vm.DeclInject }
func (c *captureDecl[V]) Name() string                   { return c.name }
func (c *captureDecl[V]) Set(vm.Context, vm.Value) error { return vm.ErrImmutable }
