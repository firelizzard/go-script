package libs

import (
	"fmt"
	"io"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/inject"
)

var Fmt = inject.Package("fmt", "fmt", map[string]any{
	"Errorf": fmt.Errorf,

	"Println": print(fmt.Fprintln),
	"Print":   print(fmt.Fprint),
	"Printf":  printf(fmt.Fprintf),

	"Sprintln": fmt.Sprintln,
	"Sprint":   fmt.Sprint,
	"Sprintf":  fmt.Sprintf,
})

func print(fn func(io.Writer, ...any) (int, error)) func(vm.Context, ...any) {
	return func(ctx vm.Context, a ...any) {
		fn(inject.ResolveStdout(ctx), a...)
	}
}

func printf(fn func(io.Writer, string, ...any) (int, error)) func(vm.Context, string, ...any) {
	return func(ctx vm.Context, s string, a ...any) {
		fn(inject.ResolveStdout(ctx), s, a...)
	}
}
