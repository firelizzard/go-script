package inject

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type goPackage struct {
	name    string
	pkgPath string
	decls   map[string]vm.Declaration
}

type goDecl struct {
	val  goValue
	name string
}

func (p *goPackage) Kind() vm.DeclKind              { return vm.DeclInject }
func (p *goPackage) Name() string                   { return p.name }
func (p *goPackage) PkgPath() string                { return p.pkgPath }
func (p *goPackage) Type() vm.Type                  { return common.TypeFor(p) }
func (p *goPackage) Set(vm.Context, vm.Value) error { return vm.ErrImmutable }

func (d *goDecl) Type() vm.Type                       { return d.val.Type() }
func (d *goDecl) Kind() vm.DeclKind                   { return vm.DeclInject }
func (d *goDecl) Name() string                        { return d.name }
func (d *goDecl) Set(vm.Context, vm.Value) error      { return vm.ErrImmutable }
func (d *goDecl) Unwrap(vm.Context) (vm.Value, error) { return d.val, nil }

func (p *goPackage) Resolve(_ vm.Context, name string) (vm.Declaration, error) {
	if d, ok := p.decls[name]; ok {
		return d, nil
	}
	return nil, common.SymbolNotFound(name)
}
