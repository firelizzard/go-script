package inject

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

func (v goValue) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if m := v.val.MethodByName(name); m.IsValid() {
		return &goDecl{val: goValue{m}, name: name}, nil
	}

	switch v.val.Kind() {
	case reflect.Pointer:
		return goValue{v.val.Elem()}.Resolve(ctx, name)
	case reflect.Struct:
		// Ok
	default:
		return nil, common.NotAMember(name, v.val.Type())
	}

	f, ok := v.val.Type().FieldByName(name)
	if !ok {
		return nil, common.NotAMember(name, v.val.Type())
	}

	u := goValue{v.val.FieldByIndex(f.Index)}
	return &goField{typ: f.Type, val: u, name: name}, nil
}

type goField struct {
	typ  reflect.Type
	val  goValue
	name string
}

func (d *goField) Type() vm.Type                       { return d.val.Type() }
func (d *goField) Kind() vm.DeclKind                   { return vm.DeclVar }
func (d *goField) Name() string                        { return d.name }
func (d *goField) Unwrap(vm.Context) (vm.Value, error) { return d.val, nil }

func (d *goField) Set(ctx vm.Context, v vm.Value) error {
	rv := asNativeValue(ctx, v)
	if !rv.Type().AssignableTo(d.typ) {
		return common.CannotConvert(v.Type(), d.typ)
	}
	d.val.val.Set(rv)
	return nil
}
