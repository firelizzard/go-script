package inject

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type goValue struct{ val reflect.Value }
type goType struct{ typ reflect.Type }

func (v goValue) Type() vm.Type                                { return goType{typ: v.val.Type()} }
func (v goValue) NativeValue(vm.Context) (reflect.Value, bool) { return v.val, true }
func (v goValue) Unwrap(vm.Context) (vm.Value, error)          { return Value(v), nil }

func (t goType) String() string                   { return t.typ.String() }
func (t goType) Type() vm.Type                    { return common.TypeFor(t) }
func (t goType) NativeType() (reflect.Type, bool) { return t.typ, true }
func (t goType) New(vm.Context) (vm.Value, error) { return goValue{val: reflect.Zero(t.typ)}, nil }

func (t goType) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	nt, ok := common.NativeType(ctx, typ)
	if !ok {
		return false, nil
	}
	return t.typ.AssignableTo(nt), nil
}

func (t goType) Convert(ctx vm.Context, v vm.Value, coerce bool) (vm.Value, error) {
	// TODO Coerce

	rv := asNativeValue(ctx, v)
	if !rv.Type().AssignableTo(t.typ) {
		return nil, common.CannotConvert(v.Type(), t)
	}

	if t.typ.Kind() == reflect.Interface {
		return v, nil
	}

	u := reflect.New(t.typ).Elem()
	u.Set(rv)
	return Value(u), nil
}

func asNativeValue(ctx vm.Context, v vm.Value) reflect.Value {
	if v, ok := common.NativeValue(ctx, v); ok {
		return v
	}
	return reflect.ValueOf(v)
}
