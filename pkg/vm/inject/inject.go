package inject

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func Package(name, pkgPath string, members map[string]any) vm.Package {
	pkg := new(goPackage)
	pkg.name = name
	pkg.pkgPath = pkgPath
	pkg.decls = map[string]vm.Declaration{}

	for name, value := range members {
		rv, ok := value.(reflect.Value)
		if !ok {
			rv = reflect.ValueOf(value)
		}
		pkg.decls[name] = &goDecl{val: goValue{rv}, name: name}
	}
	return pkg
}

var typeVmValue = reflect.TypeFor[vm.Value]()

func Value(v any) vm.Value {
	if v == nil {
		return common.Nil{}
	}
	if v, ok := v.(vm.Value); ok {
		return v
	}

	rv, ok := v.(reflect.Value)
	if !ok {
		rv = reflect.ValueOf(v)
	}

	for {
		if rv.Type().AssignableTo(typeVmValue) {
			return rv.Interface().(vm.Value)
		}
		if rv.Type().NumMethod() > 0 {
			return goValue{rv}
		}
		switch rv.Kind() {
		case reflect.Int:
			return values.Int{V: int(rv.Int())}
		case reflect.Int8:
			return values.Int8{V: int8(rv.Int())}
		case reflect.Int16:
			return values.Int16{V: int16(rv.Int())}
		case reflect.Int32:
			return values.Int32{V: int32(rv.Int())}
		case reflect.Int64:
			return values.Int64{V: int64(rv.Int())}
		case reflect.Uint:
			return values.Uint{V: uint(rv.Uint())}
		case reflect.Uint8:
			return values.Uint8{V: uint8(rv.Uint())}
		case reflect.Uint16:
			return values.Uint16{V: uint16(rv.Uint())}
		case reflect.Uint32:
			return values.Uint32{V: uint32(rv.Uint())}
		case reflect.Uint64:
			return values.Uint64{V: uint64(rv.Uint())}
		case reflect.Uintptr:
			return values.Uintptr{V: uintptr(rv.Uint())}
		case reflect.Float32:
			return values.Float32{V: float32(rv.Float())}
		case reflect.Float64:
			return values.Float64{V: float64(rv.Float())}

		case reflect.Bool:
			return values.Bool{V: rv.Bool()}
		case reflect.String:
			return values.String{V: rv.String()}

		case reflect.Interface:
			if rv.IsNil() {
				return common.Nil{}
			}
			if rv.Type().NumMethod() == 0 {
				rv = rv.Elem()
				continue
			}
			fallthrough

		case reflect.Complex64:
			fallthrough
		case reflect.Complex128:
			fallthrough
		case reflect.Array:
			fallthrough
		case reflect.Chan:
			fallthrough
		case reflect.Map:
			fallthrough
		case reflect.Pointer:
			fallthrough
		case reflect.Slice:
			fallthrough
		case reflect.Struct:
			fallthrough

		default:
			return goValue{val: rv}
		}
	}
}
