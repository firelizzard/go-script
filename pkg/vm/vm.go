package vm

// Yaegi uses genInterfaceWrapper to convert a VM value into a real Go interface
// value, if the interface type is known (otherwise it just panics on nil)

type DeclKind int

const (
	DeclGlobal DeclKind = iota
	DeclInject
	DeclPackage
	DeclImport
	DeclType
	DeclFunc
	DeclVar
	DeclConst
)

type Package interface {
	Declaration
	Resolver
	PkgPath() string
}

type Resolver interface {
	Resolve(Context, string) (Declaration, error)
}

type Scope interface {
	Resolver
	Declare(Context, Declaration) error
}

type Wrapped interface {
	Unwrap(Context) (Value, error)
}
type Bindable interface {
	Bind(Context) (Value, error)
}

type Declaration interface {
	Value
	Kind() DeclKind
	Name() string
	Set(Context, Value) error
}

type Op interface {
	Value
	Execute(Context) error
}


type Context interface {
	New() Context
	World() Scope
	Stack() StackWithFrame[Value]
	Scope() Stack[Scope]

	// Load loads the package at the given path
	Load(path string) (Package, error)
}

type Stack[V any] interface {
	Len() int
	Push(v ...V)
	Peek() V
	Pop() V
	PopN(n int) []V
	PopI(i int) []V
	Find(func(V) bool) (V, bool)
}

type StackWithFrame[V any] interface {
	Stack[V]
	Frame() StackFramer
}

type StackFramer interface {
	Push()
	Pop()
}
