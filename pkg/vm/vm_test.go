package vm_test

import (
	"fmt"
	"testing"

	"go/parser"
	"go/token"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/go-script/pkg/vm/program"
)

var src = `
package main

import "fmt"

type Bar struct {
	A, B Foo
}

func foo(c Foo) (d Foo) {
	return c * 2
}

type Foo int

func main() {
	var a, b Foo = 1, 2
	f := Bar{a, b}
	f.B = 3
	x := f.A + foo(f.B)
	err := fmt.Errorf("%d", int(x))
	fmt.Println(err.Error())
}
`

func TestSimple(t *testing.T) {
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "main.go", src, 0)
	require.NoError(t, err)

	world := program.NewWorld()
	pkg := world.NewPackage("main", "foo/bar/main")
	err = pkg.NewFile("main.go").ParseAST(f)
	require.NoError(t, err)

	v, err := pkg.Call("main")
	require.NoError(t, err)
	for _, v := range v {
		fmt.Println(v)
	}
}
