package program

import (
	"sync/atomic"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
)

type initializer atomic.Pointer[execute.Program]

func (i *initializer) a() *atomic.Pointer[execute.Program] {
	return (*atomic.Pointer[execute.Program])(i)
}

func (i *initializer) Set(ops execute.Program) {
	i.a().Store(&ops)
}

func (i *initializer) Append(ops ...vm.Value) {
	p := i.a()
	init := p.Load()
	if init == nil {
		init = new(execute.Program)
	}
	*init = append(*init, ops...)
	p.Store(init)
}

func (i *initializer) run(ctx vm.Context) (bool, error) {
	// Atomically load and clear init
again:
	init := i.a().Load()
	if init == nil {
		return false, nil
	}
	if !i.a().CompareAndSwap(init, nil) {
		goto again
	}

	return true, (*init).Execute(ctx)
}
