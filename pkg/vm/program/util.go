package program

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type opValue struct{}

func (opValue) Type() vm.Type { return common.TypeOf[vm.Op]{} }
