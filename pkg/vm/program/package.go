package program

import (
	"fmt"
	"sync/atomic"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

type vmPackage struct {
	world   *World
	name    string
	pkgPath string
	init    initializer
	sealed  atomic.Bool
	decls   map[string]vm.Declaration
}

type File struct {
	pkg     *vmPackage
	name    string
	imports map[string]string
}

func (w *World) NewPackage(name, path string) *vmPackage {
	pkg := new(vmPackage)
	pkg.name = name
	pkg.pkgPath = path
	pkg.decls = map[string]vm.Declaration{}
	w.addPackage(pkg, true)
	return pkg
}

func (w *World) LoadOrCreatePackage(name, path string) *vmPackage {
	pkg, ok := w.addPackage(&vmPackage{
		name:    name,
		pkgPath: path,
		decls:   map[string]vm.Declaration{},
	}, false).(*vmPackage)
	if !ok {
		panic(fmt.Errorf("%s has already been added to this world but is not a %T", path, (*vmPackage)(nil)))
	}
	return pkg
}

func (p *vmPackage) NewFile(name string) *File {
	f := new(File)
	f.pkg = p
	f.name = name
	f.imports = map[string]string{}
	return f
}

func (p *vmPackage) Kind() vm.DeclKind              { return vm.DeclPackage }
func (p *vmPackage) Name() string                   { return p.name }
func (p *vmPackage) PkgPath() string                { return p.pkgPath }
func (p *vmPackage) Type() vm.Type                  { return common.TypeFor(p) }
func (p *vmPackage) Set(vm.Context, vm.Value) error { return vm.ErrImmutable }

func (f *File) Declare(vm.Context, vm.Declaration) error { return vm.ErrSealed }

func (p *vmPackage) Declare(_ vm.Context, decl vm.Declaration) error {
	if p.sealed.Load() {
		return vm.ErrSealed
	}

	if _, ok := p.decls[decl.Name()]; ok {
		return fmt.Errorf("invalid attempt to redefine %v", decl.Name())
	}
	p.decls[decl.Name()] = decl
	return nil
}

func (p *vmPackage) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if d, ok := p.decls[name]; ok {
		return d, nil
	}
	return p.world.Resolve(ctx, name)
}

func (f *File) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	pkgPath, ok := f.imports[name]
	if !ok {
		return f.pkg.Resolve(ctx, name)
	}
	return f.pkg.world.Package(pkgPath), nil
}

func (p *vmPackage) Unwrap(ctx vm.Context) (vm.Value, error) {
	// Execute init
	ctx = ctx.New()
	ctx.Scope().Push(p)
	ok, err := p.init.run(ctx)
	if err != nil {
		return nil, err
	}

	// Seal the package (unless this is package main)
	if ok && p.name != "main" {
		p.sealed.Store(true)
	}

	return p, nil
}

func (p *vmPackage) Call(name string, args ...vm.Value) ([]vm.Value, error) {
	ctx := p.world.NewContext()
	err := execute.Program{
		execute.Program(args).AsLiteral(),
		p,
		values.String{V: name},
		&execute.Select{},
		&values.OpCall{},
	}.Execute(ctx)
	if err != nil {
		return nil, err
	}

	return ctx.Stack().PopI(0), nil
}
