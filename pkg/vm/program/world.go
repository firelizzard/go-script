package program

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/inject/libs"
)

type World struct {
	packages map[string]vm.Package
}

func NewWorld() *World {
	w := new(World)
	w.packages = map[string]vm.Package{}
	w.addPackage(libs.Fmt, true)
	return w
}

func (w *World) Kind() vm.DeclKind                        { return vm.DeclGlobal }
func (w *World) Name() string                             { return "(world)" }
func (w *World) Declare(vm.Context, vm.Declaration) error { return vm.ErrSealed }

func (w *World) NewContext() vm.Context {
	ctx := execute.NewContext(execute.Options{
		World: w,
		Load: func(path string) (vm.Package, error) {
			p, ok := w.packages[path]
			if ok {
				return p, nil
			}
			return nil, fmt.Errorf("%q is not a package", path)
		},
	})
	ctx.Scope().Push(w)
	return ctx
}

func (w *World) addPackage(p vm.Package, create bool) vm.Package {
	if p, ok := p.(*vmPackage); ok {
		if p.world == w {
			return p
		}
		if p.world != nil {
			panic("package has been registered with a different program")
		}
		p.world = w
	}
	if q, ok := w.packages[p.PkgPath()]; !ok {
		w.packages[p.PkgPath()] = p
		return p
	} else if !create {
		return q
	} else {
		panic("attempted to register duplicate package path")
	}
}

func (w *World) Package(path string) vm.Package {
	pkg, ok := w.packages[path]
	if !ok {
		panic(fmt.Errorf("cannot resolve package %v", path))
	}
	return pkg
}

func (w *World) Resolve(_ vm.Context, name string) (vm.Declaration, error) {
	if decl, ok := execute.ResolveBuiltin(name); ok {
		return decl, nil
	}
	return nil, common.SymbolNotFound(name)
}
