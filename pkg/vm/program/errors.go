package program

import (
	"errors"
	"fmt"
	"go/ast"
)

type errList []error

func (e *errList) ok() bool { return e == nil || len(*e) == 0 }

func (e *errList) add(node ast.Node, err error) *errList {
	if err == nil {
		return e
	}
	err = &ParseError{Wrapped: err, Node: node}
	if e == nil {
		return &errList{err}
	}
	*e = append(*e, err)
	return e
}

func (e *errList) addf(node ast.Node, format string, args ...any) *errList {
	return e.add(node, fmt.Errorf(format, args...))
}

func (e *errList) join() error {
	return errors.Join(*e...)
}

type ParseError struct {
	Wrapped error
	Node    ast.Node
}

func (e *ParseError) Error() string {
	return e.Wrapped.Error()
}

func (e *ParseError) Unwrap() error {
	return e.Wrapped
}
