package program

import (
	"fmt"
	"go/ast"

	"gitlab.com/firelizzard/go-script/pkg/vm/parse"
	"golang.org/x/tools/go/packages"
)

func (p *World) Parse(src *packages.Package) (*vmPackage, error) {
	pkg := p.NewPackage(src.Name, src.PkgPath)

	for _, f := range src.Syntax {
		err := pkg.NewFile(f.Name.Name).ParseAST(f)
		if err != nil {
			return nil, err
		}
	}
	return pkg, nil
}

func (f *File) ParseAST(src ast.Node) error {
	switch src := src.(type) {
	case *ast.File:
		return f.parseFile(src)
	case ast.Decl:
		return f.parseDecl(src)
	default:
		return fmt.Errorf("%T is not a declaration", src)
	}
}

func (f *File) parseFile(src *ast.File) error {
	// for _, is := range src.Imports {
	// 	if is.Path.Kind != token.STRING {
	// 		panic("import value is not a string")
	// 	}
	// 	path, err := strconv.Unquote(is.Path.Value)
	// 	if err != nil {
	// 		panic(fmt.Errorf("invalid import: %w", err))
	// 	}
	// 	f.imports[is.Name.Name] = path
	// }

	for _, decl := range src.Decls {
		err := f.parseDecl(decl)
		if err != nil {
			return err
		}
	}

	return nil
}

func (f *File) parseDecl(src ast.Decl) error {
	ops, err := parse.Parse(parse.Decl, src)
	if err != nil {
		return err
	}
	f.pkg.init.Append(ops...)
	return nil
}
