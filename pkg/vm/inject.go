package vm

import (
	"reflect"
)

// NativeType is a [Type] that can be converted to a native Go type.
type NativeType interface {
	Type
	NativeType(Context) (reflect.Type, bool)
}

// NativeValue is a [Value] that can be converted to a native Go value.
type NativeValue interface {
	Value
	NativeValue(Context) (reflect.Value, bool)
}
