package vm

import "errors"

var ErrNotFound = errors.New("not found")
var ErrSealed = errors.New("scope is sealed")
var ErrImmutable = errors.New("declaration is immutable")
var ErrCannotConvert = errors.New("cannot convert")
var ErrWrongType = errors.New("wrong type")
