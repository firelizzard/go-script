package common

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

func NativeType(ctx vm.Context, typ vm.Type) (reflect.Type, bool) {
	gt, ok := typ.(vm.NativeType)
	if !ok {
		return nil, false
	}
	return gt.NativeType(ctx)
}

func NativeValue(ctx vm.Context, value vm.Value) (reflect.Value, bool) {
	gv, err := Unwrap[vm.NativeValue](ctx, value)
	if err != nil {
		return reflect.Value{}, false
	}
	return gv.NativeValue(ctx)
}
