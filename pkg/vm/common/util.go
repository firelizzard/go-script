package common

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

func PopUnwrapAs[V any, U any](ctx vm.Context, as func(V) U) (U, error) {
	v, err := Unwrap[V](ctx, ctx.Stack().Pop())
	if err != nil {
		var z U
		return z, err
	}
	return as(v), nil
}

func UnwrapAs[V any, U any](ctx vm.Context, v vm.Value, as func(V) U) (U, error) {
	v2, err := Unwrap[V](ctx, v)
	if err != nil {
		var z U
		return z, err
	}
	return as(v2), nil
}

func PopUnwrapEarlyAs[V any, U any](ctx vm.Context, as func(V) U) (U, error) {
	v, err := UnwrapEarly[V](ctx, ctx.Stack().Pop())
	if err != nil {
		var z U
		return z, err
	}
	return as(v), nil
}

func UnwrapEarlyAs[V any, U any](ctx vm.Context, v vm.Value, as func(V) U) (U, error) {
	v2, err := UnwrapEarly[V](ctx, v)
	if err != nil {
		var z U
		return z, err
	}
	return as(v2), nil
}

func PopUnwrap[V any](ctx vm.Context) (V, error) {
	return Unwrap[V](ctx, ctx.Stack().Pop())
}

func PopUnwrapEarly[V any](ctx vm.Context) (V, error) {
	return UnwrapEarly[V](ctx, ctx.Stack().Pop())
}

func Unwrap[V any](ctx vm.Context, v vm.Value) (V, error) {
	return unwrap[V](ctx, v, false)
}

func UnwrapEarly[V any](ctx vm.Context, v vm.Value) (V, error) {
	return unwrap[V](ctx, v, true)
}

func unwrap[V any](ctx vm.Context, v vm.Value, early bool) (V, error) {
	var z V
	for {
		if early {
			if v, ok := v.(V); ok {
				return v, nil
			}
		}
		u, ok := v.(vm.Wrapped)
		if !ok {
			break
		}
		w, err := u.Unwrap(ctx)
		if err != nil {
			return z, err
		}
		if w == v {
			break
		}
		v = w
	}

	if v, ok := v.(V); ok {
		return v, nil
	}

	return z, WrongType(reflect.TypeFor[V](), reflect.TypeOf(v))
}

func Pop[V any](ctx vm.Context) (V, error) {
	v := ctx.Stack().Pop()
	if v, ok := v.(V); ok {
		return v, nil
	}

	var z V
	return z, WrongType(reflect.TypeFor[V](), reflect.TypeOf(v))
}

func Underlying(v vm.Value) vm.Value {
	for {
		if u, ok := v.(vm.Named); ok {
			v = u.Underlying()
		} else {
			return v
		}
	}
}
