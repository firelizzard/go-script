package common

import "gitlab.com/firelizzard/go-script/pkg/vm"

func ExecuteInFrame(ctx vm.Context, stmt vm.Op) ([]vm.Value, error) {
	// Push a stack frame
	ctx.Stack().Frame().Push()
	defer ctx.Stack().Frame().Pop()

	// Execute the statement
	err := stmt.Execute(ctx)
	if err != nil {
		return nil, err
	}

	// Pop all values
	return ctx.Stack().PopI(0), nil
}

func PopAndExecuteInFrame(ctx vm.Context) ([]vm.Value, error) {
	// Pop the statement
	stmt, err := PopUnwrap[vm.Op](ctx)
	if err != nil {
		return nil, err
	}

	// And execute it
	return ExecuteInFrame(ctx, stmt)
}
