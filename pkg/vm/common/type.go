package common

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

func TypeFor[V vm.Value](V) TypeOf[V] { return TypeOf[V]{} }

type TypeOf[V vm.Value] struct{}

func (t TypeOf[V]) Type() vm.Type  { return TypeOf[vm.Type]{} }
func (t TypeOf[V]) String() string { return reflect.TypeFor[V]().String() }

func (t TypeOf[V]) New(vm.Context) (vm.Value, error) {
	rt := reflect.TypeFor[V]()
	if rt.Kind() == reflect.Pointer {
		return reflect.New(rt.Elem()).Interface().(V), nil
	}

	var v V
	return v, nil
}

func (t TypeOf[V]) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	return nil, CannotConvert(v.Type(), t.Type())
}

func (t TypeOf[V]) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	return false, nil
}

func UnderlyingType(ctx vm.Context, typ vm.Type) (vm.Type, error) {
	for {
		switch t := typ.(type) {
		case vm.NamedType:
			typ = t.Underlying()
		case vm.Wrapped:
			v, err := t.Unwrap(ctx)
			if err != nil {
				return nil, err
			}
			if t == any(v) {
				return typ, nil
			}
			if t, ok := v.(vm.Type); !ok {
				return typ, nil
			} else {
				typ = t
			}
		default:
			return typ, nil
		}
	}
}
