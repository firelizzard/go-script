package common

type Stack[V any] []V

func (s *Stack[V]) Len() int {
	if s == nil {
		return 0
	}
	return len(*s)
}

func (s *Stack[V]) Push(v ...V) {
	s.Push2(v...)
}

func (s *Stack[V]) Push2(v ...V) *Stack[V] {
	if s == nil {
		s := Stack[V](v)
		return &s
	}
	*s = append(*s, v...)
	return s
}

func (s *Stack[V]) Peek() V {
	if s.Len() == 0 {
		panic("stack underflow")
	}
	return (*s)[len(*s)-1]
}

func (s *Stack[V]) Pop() V {
	return s.PopN(1)[0]
}

func (s *Stack[V]) PopN(n int) []V {
	return s.PopI(s.Len() - n)
}

func (s *Stack[V]) PopI(i int) []V {
	if i < 0 {
		panic("stack underflow")
	}
	if i == 0 && s == nil {
		return nil
	}
	t := make([]V, s.Len()-i)
	copy(t, (*s)[i:])
	*s = (*s)[:i]
	return t
}

func (s *Stack[V]) Find(fn func(V) bool) (V, bool) {
	for i := len(*s) - 1; i >= 0; i-- {
		if fn((*s)[i]) {
			return (*s)[i], true
		}
	}
	var z V
	return z, false
}
