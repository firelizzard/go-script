package common

import (
	"errors"
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

func OperatorNotDefined(name string, typ vm.Type) error {
	return fmt.Errorf("operator %s not defined for %v", name, typ)
}

func SymbolNotFound(name string) error {
	return fmt.Errorf("symbol %q %w", name, vm.ErrNotFound)
}

func WrongType(want, got any) error {
	return fmt.Errorf("%w: want %v, got %v", vm.ErrWrongType, want, got)
}

func CannotConvert(from, to any) error {
	return fmt.Errorf("%w %v to %v", vm.ErrCannotConvert, from, to)
}

func NotAMember(name string, typ any) error {
	return &silentWrappedError{
		message: fmt.Sprintf("%s is not a member of %v", name, typ),
		wrapped: vm.ErrNotFound,
	}
}

func CheckValueCount(got, want int, variadic bool) error {
	switch {
	case variadic && got < want-1:
		return fmt.Errorf("too few values: must have at least %d, got %d", want-1, got)
	case variadic:
		return nil
	case got < want:
		return fmt.Errorf("too few values: want %d, got %d", want, got)
	case got > want:
		return fmt.Errorf("too many values: want %d, got %d", want, got)
	}
	return nil
}

type silentWrappedError struct {
	message string
	wrapped error
}

func (s *silentWrappedError) Error() string { return s.message }
func (s *silentWrappedError) Unwrap() error { return s.wrapped }

func ErrorAs[V any](err error) (V, bool) {
	var v V
	is := errors.As(err, &v)
	return v, is
}
