package common

import "gitlab.com/firelizzard/go-script/pkg/vm"

type ErrReturn []vm.Value
type ErrBreak string
type ErrContinue string

func (ErrReturn) Error() string   { return "inappropriate use of return" }
func (ErrBreak) Error() string    { return "inappropriate use of break" }
func (ErrContinue) Error() string { return "inappropriate use of continue" }

type OpReturn struct{}
type OpBreak struct{}
type OpBreakTo struct{}
type OpContinue struct{}
type OpContinueTo struct{}

var (
	_ vm.Op = (*OpReturn)(nil)
	_ vm.Op = (*OpBreak)(nil)
	_ vm.Op = (*OpBreakTo)(nil)
	_ vm.Op = (*OpContinue)(nil)
	_ vm.Op = (*OpContinueTo)(nil)
)

func (o *OpReturn) Type() vm.Type     { return TypeFor(o) }
func (o *OpBreak) Type() vm.Type      { return TypeFor(o) }
func (o *OpBreakTo) Type() vm.Type    { return TypeFor(o) }
func (o *OpContinue) Type() vm.Type   { return TypeFor(o) }
func (o *OpContinueTo) Type() vm.Type { return TypeFor(o) }

func (o *OpBreak) Execute(ctx vm.Context) error    { return ErrBreak("") }
func (o *OpContinue) Execute(ctx vm.Context) error { return ErrContinue("") }

func (o *OpReturn) Execute(ctx vm.Context) error {
	// Pop the return statement and execute it
	values, err := PopAndExecuteInFrame(ctx)
	if err != nil {
		return err
	}

	// Return the values
	return ErrReturn(values)
}

func (o *OpBreakTo) Execute(ctx vm.Context) error {
	s, err := PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	return ErrBreak(s)
}

func (o *OpContinueTo) Execute(ctx vm.Context) error {
	s, err := PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	return ErrContinue(s)
}
