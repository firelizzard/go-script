package common

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

type blockScope struct {
	parent vm.Scope
	decls  map[string]vm.Declaration
}

func NewBlockScope(parent vm.Scope) vm.Scope {
	return &blockScope{
		parent: parent,
		decls:  map[string]vm.Declaration{},
	}
}

func (s *blockScope) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if d, ok := s.decls[name]; ok {
		return d, nil
	}
	return s.parent.Resolve(ctx, name)
}

func (s *blockScope) Declare(_ vm.Context, decl vm.Declaration) error {
	name := decl.Name()
	if _, ok := s.decls[name]; ok {
		return fmt.Errorf("%v has already been declared", name)
	}
	s.decls[name] = decl
	return nil
}
