package common

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

type Nil struct {
	// TODO: Nil should probably be an untyped constant
}

func (Nil) Type() vm.Type { return Any{} }
func (Nil) NativeValue(vm.Context) (reflect.Value, bool) {
	return reflect.ValueOf(new(any)).Elem(), true
}

type Any struct{}

func (t Any) Type() vm.Type                    { return TypeOf[vm.Type]{} }
func (t Any) String() string                   { return "any" }
func (t Any) NativeType() reflect.Type         { return reflect.TypeFor[any]() }
func (t Any) New(vm.Context) (vm.Value, error) { return Nil{}, nil }

func (t Any) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	return v, nil
}

func (t Any) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	if typ == (Any{}) {
		return true, nil
	}

	// TODO: Assign nil to a pointer?
	return false, nil
}
