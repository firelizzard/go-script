package common

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
)

type OpDup struct{}
type OpSwap struct{}
type OpPush struct{ vm.Value }

var (
	_ vm.Op = (*OpDup)(nil)
	_ vm.Op = (*OpSwap)(nil)
)

func (o *OpDup) Type() vm.Type  { return TypeFor(o) }
func (o *OpSwap) Type() vm.Type { return TypeFor(o) }

func (o *OpDup) Execute(ctx vm.Context) error {
	ctx.Stack().Push(ctx.Stack().Peek())
	return nil
}

func (o *OpSwap) Execute(ctx vm.Context) error {
	v := ctx.Stack().PopN(2)
	ctx.Stack().Push(v[1], v[0])
	return nil
}

func (p *OpPush) Execute(ctx vm.Context) error {
	ctx.Stack().Push(p.Value)
	return nil
}
