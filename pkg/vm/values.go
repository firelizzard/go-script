package vm

type Type interface {
	Value
	New(Context) (Value, error)
	AssignableTo(Context, Type) (bool, error)
	Convert(ctx Context, value Value, coerce bool) (Value, error)
}

type Untype interface {
	Type
	Concrete() Type
}

type Untyped[V any] interface {
	Value
	AsUntyped() V
}

type Value interface {
	Type() Type
}

type NamedType interface {
	Underlying() Type
}

type Named interface {
	Value
	Underlying() Value
}

type FuncType interface {
	Type
	NumIn() int
	NumOut() int
	IsVariadic() bool
	In(int) Type
	Out(int) Type
}

type Func interface {
	Value
	Call(Context, []Value) ([]Value, error)
}

type Bool interface {
	Value
	AsBool() bool
}

type Int interface {
	Value
	AsInt() int64
}

type Uint interface {
	Value
	AsUint() uint64
}

type Float interface {
	Value
	AsFloat() float64
}

type String interface {
	Value
	AsString() string
}

type ElemType interface {
	Type
	Elem() Type
}
