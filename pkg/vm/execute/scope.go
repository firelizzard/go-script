package execute

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type Declare struct {
	opValue
	Count int
}

func (o *Declare) Execute(ctx vm.Context) error {
	if o.Count == 0 {
		o.Count = 1
	}

	for range o.Count {
		decl, err := common.Pop[vm.Declaration](ctx)
		if err != nil {
			return err
		}
		if decl.Name() == "_" {
			continue
		}
		if b, ok := decl.(vm.Bindable); ok {
			v, err := b.Bind(ctx)
			if err != nil {
				return err
			}
			decl = v.(vm.Declaration)
		}
		err = ctx.Scope().Peek().Declare(ctx, decl)
		if err != nil {
			return err
		}
	}
	return nil
}

type Assign struct {
	opValue
	Count int
}

func (*Assign) Execute(ctx vm.Context) error {
	decls, err := popExecPop[vm.Declaration](ctx, true)
	if err != nil {
		return err
	}

	values, err := popExecPop[vm.Value](ctx, false)
	if err != nil {
		return err
	}

	err = common.CheckValueCount(len(values), len(decls), false)
	if err != nil {
		return err
	}

	for i, decl := range decls {
		err = decl.Set(ctx, values[i])
		if err != nil {
			return err
		}
	}

	return nil
}

type Resolve struct{ opValue }

func (o *Resolve) Execute(ctx vm.Context) error {
	name, err := common.PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	decl, err := ctx.Scope().Peek().Resolve(ctx, name)
	if err != nil {
		return err
	}
	ctx.Stack().Push(decl)
	return nil
}

type Select struct{ opValue }

func (o *Select) Execute(ctx vm.Context) error {
	name, err := common.PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	v, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return err
	}
	r, ok := v.(vm.Resolver)
	if !ok {
		return fmt.Errorf("invalid attempt to access a member of a %T", v)
	}
	decl, err := r.Resolve(ctx, name)
	if err != nil {
		return err
	}
	ctx.Stack().Push(decl)
	return nil
}

func popExecPop[V any](ctx vm.Context, early bool) ([]V, error) {
	raw, err := common.PopAndExecuteInFrame(ctx)
	if err != nil {
		return nil, err
	}

	// Pop all values
	values := make([]V, len(raw))
	for i, value := range raw {
		if early {
			values[i], err = common.UnwrapEarly[V](ctx, value)
		} else {
			values[i], err = common.Unwrap[V](ctx, value)
		}
		if err != nil {
			return nil, err
		}
	}
	return values, nil
}
