package execute

import (
	"errors"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type OpBlock struct{}
type OpFor struct{}
type OpIf struct{}

var _ vm.Op = (*OpBlock)(nil)
var _ vm.Op = (*OpFor)(nil)
var _ vm.Op = (*OpIf)(nil)

func (o *OpBlock) Type() vm.Type { return common.TypeFor(o) }
func (o *OpFor) Type() vm.Type   { return common.TypeFor(o) }
func (o *OpIf) Type() vm.Type    { return common.TypeFor(o) }

func (o *OpBlock) Execute(ctx vm.Context) error {
	body, err := common.PopUnwrap[vm.Op](ctx)
	if err != nil {
		return err
	}

	ctx.Scope().Push(common.NewBlockScope(ctx.Scope().Peek()))
	defer ctx.Scope().Pop()

	ctx.Stack().Frame().Push()
	defer ctx.Stack().Frame().Pop()

	return body.Execute(ctx)
}

func (o *OpFor) Execute(ctx vm.Context) error {
	post, e1 := common.PopUnwrap[vm.Op](ctx)
	body, e2 := common.PopUnwrap[vm.Op](ctx)
	cond, e3 := common.PopUnwrap[vm.Op](ctx)
	init, e4 := common.PopUnwrap[vm.Op](ctx)
	if err := errors.Join(e1, e2, e3, e4); err != nil {
		return err
	}

	// Block
	ctx.Scope().Push(common.NewBlockScope(ctx.Scope().Peek()))
	defer ctx.Scope().Pop()

	ctx.Stack().Frame().Push()
	defer ctx.Stack().Frame().Pop()

	var err error
	for err = init.Execute(ctx); err == nil; err = post.Execute(ctx) {
		// Condition
		err := cond.Execute(ctx)
		if err != nil {
			return err
		}
		ok, err := common.PopUnwrap[vm.Bool](ctx)
		if err != nil {
			return err
		}
		if !ok.AsBool() {
			break
		}

		// Body
		err = body.Execute(ctx)
		if err == nil {
			continue
		} else if _, ok := common.ErrorAs[common.ErrBreak](err); ok {
			break
		} else if _, ok := common.ErrorAs[common.ErrContinue](err); ok {
			continue
		} else {
			return err
		}
	}
	if err != nil {
		return err
	}

	return nil
}

func (o *OpIf) Execute(ctx vm.Context) error {
	else_, e1 := common.PopUnwrap[vm.Op](ctx)
	body, e2 := common.PopUnwrap[vm.Op](ctx)
	cond, e3 := common.PopUnwrap[vm.Op](ctx)
	init, e4 := common.PopUnwrap[vm.Op](ctx)
	if err := errors.Join(e1, e2, e3, e4); err != nil {
		return err
	}

	// Block
	ctx.Scope().Push(common.NewBlockScope(ctx.Scope().Peek()))
	defer ctx.Scope().Pop()

	ctx.Stack().Frame().Push()
	defer ctx.Stack().Frame().Pop()

	// Init
	err := init.Execute(ctx)
	if err != nil {
		return err
	}

	// Condition
	err = cond.Execute(ctx)
	if err != nil {
		return err
	}
	ok, err := common.PopUnwrap[vm.Bool](ctx)
	if err != nil {
		return err
	}

	// Body/else
	if ok.AsBool() {
		return body.Execute(ctx)
	} else {
		return else_.Execute(ctx)
	}
}
