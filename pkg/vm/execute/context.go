package execute

import (
	"errors"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type Options struct {
	World vm.Scope
	Load  func(string) (vm.Package, error)
}

type Context struct {
	opts  Options
	scope common.Stack[vm.Scope]
	stack stack
}

func NewContext(opts Options) *Context {
	return &Context{opts: opts}
}

func (c *Context) New() vm.Context                    { return &Context{opts: c.opts} }
func (c *Context) World() vm.Scope                    { return c.opts.World }
func (c *Context) Scope() vm.Stack[vm.Scope]          { return &c.scope }
func (c *Context) Stack() vm.StackWithFrame[vm.Value] { return &c.stack }

func (c *Context) Load(path string) (vm.Package, error) {
	if c.opts.Load == nil {
		return nil, errors.New("cannot load packages: no package loader provided")
	}
	return c.opts.Load(path)
}

type PushFrame struct{ opValue }

func (*PushFrame) Execute(ctx vm.Context) error {
	ctx.Stack().Frame().Push()
	return nil
}

type PopFrame struct{ opValue }

func (*PopFrame) Execute(ctx vm.Context) error {
	ctx.Stack().Frame().Pop()
	return nil
}
