package execute

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type stack struct {
	stack common.Stack[vm.Value]
	frame common.Stack[int]
}

type stackFramer stack

func (s *stack) Frame() vm.StackFramer { return (*stackFramer)(s) }
func (s *stackFramer) Push()           { s.frame.Push(s.stack.Len()) }
func (s *stackFramer) Pop()            { s.frame.Pop() }

func (s *stack) Len() int              { return s.stack.Len() - s.start() }
func (s *stack) Push(v ...vm.Value)    { s.stack.Push(v...) }
func (s *stack) Pop() vm.Value         { return s.PopN(1)[0] }
func (s *stack) PopN(n int) []vm.Value { return s.PopI(s.Len() - n) }

func (s *stack) Peek() vm.Value {
	if s.Len() <= 0 {
		panic("stack underflow")
	}
	return s.stack.Peek()
}

func (s *stack) PopI(i int) []vm.Value {
	if i < 0 {
		panic("stack underflow")
	}
	return s.stack.PopI(s.start() + i)
}

func (s *stack) Find(fn func(vm.Value) bool) (vm.Value, bool) {
	take := s.Len() - s.start()
	return s.stack.Find(func(v vm.Value) bool {
		take--
		return take >= 0 && fn(v)
	})
}

func (s *stack) start() int {
	if s.frame.Len() == 0 {
		return 0
	}
	return s.frame.Peek()
}
