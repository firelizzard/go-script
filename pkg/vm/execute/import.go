package execute

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type OpImport struct {
	Named bool
}

var _ vm.Op = (*OpImport)(nil)

func (o *OpImport) Type() vm.Type { return common.TypeFor(o) }

func (o *OpImport) Execute(ctx vm.Context) error {
	// Get the name
	var name string
	var err error
	if o.Named {
		name, err = common.PopUnwrapEarlyAs(ctx, vm.String.AsString)
		if err != nil {
			return err
		}
	}

	// Resolve the package path
	path, err := common.PopUnwrapAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	pkg, err := ctx.Load(path)
	if err != nil {
		return err
	}

	// Use unwrap to ensure the package initializer is called
	pkg, err = common.Unwrap[vm.Package](ctx, pkg)
	if err != nil {
		return err
	}

	// Add the package to the scope
	if !o.Named {
		return ctx.Scope().Peek().Declare(ctx, pkg)
	}
	if name != "_" {
		return ctx.Scope().Peek().Declare(ctx, &namedPackage{pkg, name})
	}
	return nil
}

type namedPackage struct {
	vm.Package
	name string
}

func (n *namedPackage) Name() string { return n.name }
