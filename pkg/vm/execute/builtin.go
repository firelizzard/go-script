package execute

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"gitlab.com/firelizzard/go-script/pkg/vm/operations"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func ResolveBuiltin(name string) (vm.Declaration, bool) {
	if typ, ok := values.ResolveType(name); ok {
		return &builtin{typ, vm.DeclType, name}, true
	}

	if val, ok := values.ResolveValue(name); ok {
		return &builtin{val, vm.DeclConst, name}, true
	}

	switch name {
	case "len":
		return &builtin{_len, vm.DeclType, name}, true
	}

	return nil, false
}

func foo(fn func([]string) int) {}

var (
	_len = operations.Unary(func(v operations.LenValue) (vm.Value, error) { return values.Int{V: v.Len()}, nil }).Named("len")
)

type builtin struct {
	vm.Value
	kind vm.DeclKind
	name string
}

func (b *builtin) Kind() vm.DeclKind                         { return vm.DeclType }
func (b *builtin) Name() string                              { return b.name }
func (b *builtin) Type() vm.Type                             { return common.TypeFor(b) }
func (b *builtin) Set(vm.Context, vm.Value) error            { return vm.ErrImmutable }
func (b *builtin) Unwrap(vm.Context) (v vm.Value, err error) { return b.Value, nil }

func (b *builtin) New(ctx vm.Context) (vm.Value, error) {
	typ, ok := b.Value.(vm.Type)
	if !ok {
		return nil, fmt.Errorf("%w: %T is not a type", vm.ErrWrongType, b.Value)
	}
	return typ.New(ctx)
}

func (b *builtin) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, ok := b.Value.(vm.Type)
	if !ok {
		return false, fmt.Errorf("%w: %T is not a type", vm.ErrWrongType, b.Value)
	}
	return typ.AssignableTo(ctx, typ)
}

func (b *builtin) Convert(ctx vm.Context, value vm.Value, coerce bool) (vm.Value, error) {
	typ, ok := b.Value.(vm.Type)
	if !ok {
		return nil, fmt.Errorf("%w: %T is not a type", vm.ErrWrongType, b.Value)
	}
	return typ.Convert(ctx, value, coerce)
}
