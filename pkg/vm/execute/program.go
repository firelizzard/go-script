package execute

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type Program []vm.Value

func (p Program) Type() vm.Type    { return common.TypeFor(p) }
func (p Program) AsLiteral() vm.Op { return &common.OpPush{Value: p} }

func (p Program) Execute(ctx vm.Context) error {
	flat := p.flatten()
	for _, op := range flat {
		err := op.Execute(ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p Program) flatten() []vm.Op {
	var flat []vm.Op
	for _, v := range p {
		switch v := v.(type) {
		case Program:
			flat = append(flat, v.flatten()...)
		case vm.Op:
			flat = append(flat, v)
		default:
			flat = append(flat, &common.OpPush{Value: v})
		}
	}
	return flat
}
