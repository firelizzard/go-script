package values

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type OpCall struct{}

var _ vm.Op = (*OpCall)(nil)

func (o *OpCall) Type() vm.Type { return common.TypeFor(o) }

func (o *OpCall) Execute(ctx vm.Context) error {
	// Pop function
	v, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return err
	}

	// Pop and unwrap arguments
	args, err := common.PopAndExecuteInFrame(ctx)
	if err != nil {
		return err
	}
	for i, arg := range args {
		args[i], err = common.Unwrap[vm.Value](ctx, arg)
		if err != nil {
			return err
		}
	}

	// Call
	switch v := v.(type) {
	case vm.Func:
		out, err := v.Call(ctx, args)
		if err != nil {
			return err
		}

		// Push outputs
		ctx.Stack().Push(out...)
		return nil

	case vm.Type:
		err = common.CheckValueCount(len(args), 1, false)
		if err != nil {
			return err
		}
		u, err := v.Convert(ctx, args[0], true)
		if err != nil {
			return err
		}
		ctx.Stack().Push(u)
		return nil

	default:
		return fmt.Errorf("%T is not callable", v)
	}
}
