package values

import (
	"fmt"
	"strings"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type FunctionSig struct {
	in, out  []*fieldType
	variadic bool
}

type OpMakeFuncSig struct {
	In, Out  int
	Variadic bool
}

var _ vm.FuncType = (*FunctionSig)(nil)

func (f *FunctionSig) Type() vm.Type { return common.TypeFor(f) }

func (f *FunctionSig) NumIn() int        { return len(f.in) }
func (f *FunctionSig) NumOut() int       { return len(f.out) }
func (f *FunctionSig) IsVariadic() bool  { return f.variadic }
func (f *FunctionSig) In(i int) vm.Type  { return f.in[i].typ }
func (f *FunctionSig) Out(i int) vm.Type { return f.out[i].typ }

func (f *FunctionSig) New(vm.Context) (vm.Value, error) {
	return &typedValue{typ: f, val: common.Nil{}}, nil
}

func (f *FunctionSig) String() string {
	var in, out []string
	for _, typ := range f.in {
		in = append(in, fmt.Sprint(typ))
	}
	for _, typ := range f.out {
		out = append(out, fmt.Sprint(typ))
	}
	if f.variadic {
		i := len(out) - 1
		out[i] = "..." + out[i]
	}
	return fmt.Sprintf(
		"func(%s) (%s)",
		strings.Join(in, ", "),
		strings.Join(out, ", "),
	)
}

func (f *FunctionSig) Unwrap(ctx vm.Context) (vm.Value, error) {
	for _, in := range f.in {
		err := in.unwrap(ctx)
		if err != nil {
			return nil, err
		}
	}
	for _, out := range f.out {
		err := out.unwrap(ctx)
		if err != nil {
			return nil, err
		}
	}
	return f, nil
}

func (f *FunctionSig) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	if f == typ {
		return true, nil
	}

	// F can be assigned to G if and only if they have the same number of inputs
	// and outputs, each corresponding input and output is assignable, and both
	// are or both are not variadic

	g, ok := typ.(vm.FuncType)
	switch {
	case !ok,
		f.IsVariadic() != g.IsVariadic(),
		f.NumIn() != g.NumIn(),
		f.NumOut() != g.NumOut():
		return false, nil
	}

	for i := range f.in {
		ok, err := f.In(i).AssignableTo(ctx, g.In(i))
		if !ok || err != nil {
			return ok, err
		}
	}
	for i := range f.out {
		ok, err := f.Out(i).AssignableTo(ctx, g.Out(i))
		if !ok || err != nil {
			return ok, err
		}
	}
	return true, nil
}

func (f *FunctionSig) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	// Function types don't care about explicit vs implicit conversion
	ok, err := v.Type().AssignableTo(ctx, f)
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, common.CannotConvert(v.Type(), f)
	}

	// TODO: What if V is a named function type? Or any(func(...){...})?
	return v, nil
}

func (o *OpMakeFuncSig) Execute(ctx vm.Context) error {
	sig := new(FunctionSig)
	sig.variadic = o.Variadic
	sig.in = make([]*fieldType, o.In)
	sig.out = make([]*fieldType, o.Out)

	var err error
	for i, arg := range ctx.Stack().PopN(o.Out) {
		sig.out[i], err = unwrapFuncSigField(ctx, arg)
		if err != nil {
			return err
		}
	}
	for i, arg := range ctx.Stack().PopN(o.In) {
		sig.in[i], err = unwrapFuncSigField(ctx, arg)
		if err != nil {
			return err
		}
	}

	ctx.Stack().Push(sig)
	return nil
}

func unwrapFuncSigField(ctx vm.Context, typ vm.Value) (*fieldType, error) {
	typ, err := common.Unwrap[vm.Value](ctx, typ)
	if err != nil {
		return nil, err
	}
	switch typ := typ.(type) {
	case *fieldType:
		return typ, nil
	case vm.Type:
		return &fieldType{typ: typ}, nil
	default:
		panic(fmt.Errorf("invalid function signature type %T", typ))
	}
}
