package values

import (
	"fmt"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type field struct {
	typ *fieldType
	val vm.Value
}

type fieldType struct {
	name string
	typ  vm.Type
	tag  reflect.StructTag
}

type OpMakeFieldType struct {
	Tagged bool
}

func (f *field) Kind() vm.DeclKind                   { return vm.DeclVar }
func (f *field) Type() vm.Type                       { return f.typ.typ }
func (f *field) Name() string                        { return f.typ.name }
func (f *field) Unwrap(vm.Context) (vm.Value, error) { return f.val, nil }
func (f *fieldType) String() string                  { return fmt.Sprint(f.typ) }

func (f *fieldType) unwrap(ctx vm.Context) error {
	typ, err := common.Unwrap[vm.Type](ctx, f.typ)
	if err != nil {
		return err
	}
	f.typ = typ
	return nil
}

func (f *field) Set(ctx vm.Context, v vm.Value) error {
	v, err := f.typ.typ.Convert(ctx, v, false)
	if err != nil {
		return err
	}
	f.val = v
	return nil
}

func (f *fieldType) equal(g *fieldType) bool {
	return f.name == g.name &&
		f.typ == g.typ &&
		f.tag == g.tag
}

func (o *OpMakeFieldType) Execute(ctx vm.Context) error {
	names, err := common.PopAndExecuteInFrame(ctx)
	if err != nil {
		return err
	}
	if len(names) == 0 {
		names = append(names, String{V: "_"})
	}

	var tag string
	if o.Tagged {
		tag, err = common.PopUnwrapAs(ctx, vm.String.AsString)
		if err != nil {
			return err
		}
	}

	typ, err := popTypeOrIdent(ctx)
	if err != nil {
		return err
	}

	for _, name := range names {
		name, err := common.UnwrapEarlyAs(ctx, name, vm.String.AsString)
		if err != nil {
			return err
		}
		ctx.Stack().Push(&fieldType{
			name: name,
			typ:  typ,
			tag:  reflect.StructTag(tag),
		})
	}
	return nil
}
