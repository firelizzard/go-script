package values

import (
	"fmt"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type arrayType struct {
	elem   vm.Type
	length int
}

type sliceType struct {
	elem vm.Type
}

type arrayValue struct {
	typ    *arrayType
	values []vm.Value
}

type sliceValue struct {
	typ    *sliceType
	values []vm.Value
}

type OpMakeArrayType struct {
	Slice bool
}

func (v *arrayValue) Type() vm.Type { return v.typ }
func (v *arrayValue) Len() int      { return v.typ.length }
func (v *sliceValue) Type() vm.Type { return v.typ }
func (v *sliceValue) Len() int      { return len(v.values) }

func (t *arrayType) Elem() vm.Type  { return t.elem }
func (t *arrayType) String() string { return fmt.Sprintf("[%d]%v", t.length, t.elem) }

func (t *arrayType) New(ctx vm.Context) (vm.Value, error) {
	var err error
	v := &arrayValue{typ: t}
	v.values = make([]vm.Value, t.length)
	for i := range v.values {
		v.values[i], err = t.elem.New(ctx)
		if err != nil {
			return nil, err
		}
	}
	return v, nil
}

func (t *sliceType) Elem() vm.Type                    { return t.elem }
func (t *sliceType) New(vm.Context) (vm.Value, error) { return &sliceValue{typ: t}, nil }
func (t *sliceType) String() string                   { return fmt.Sprintf("[]%v", t.elem) }

func (v *arrayValue) Index(i vm.Value) (vm.Value, error) {
	return sliceIndex(v.typ, v.values, i)
}

func (v *sliceValue) Index(i vm.Value) (vm.Value, error) {
	return sliceIndex(v.typ, v.values, i)
}

func sliceIndex(typ vm.ElemType, slice []vm.Value, index vm.Value) (vm.Value, error) {
	var err error
	i := asInt(&err, typ, "index", index)
	if err != nil {
		return nil, err
	}
	return slice[i], nil
}

func (v *arrayValue) Slice(a, b, c vm.Value) (vm.Value, error) {
	return sliceSlice(v.typ, v.values, a, b, c)
}

func (v *sliceValue) Slice(a, b, c vm.Value) (vm.Value, error) {
	return sliceSlice(v.typ, v.values, a, b, c)
}

func sliceSlice(typ vm.ElemType, slice []vm.Value, a, b, c vm.Value) (vm.Value, error) {
	var err error
	ai := asInt(&err, typ, "slice", a)
	bi := asInt(&err, typ, "slice", b)
	ci := asInt(&err, typ, "slice", c)
	if err != nil {
		return nil, err
	}

	switch {
	case a == nil && b == nil && c == nil:
		slice = slice[:]
	case a != nil && b == nil && c == nil:
		slice = slice[ai:]
	case a == nil && b != nil && c == nil:
		slice = slice[:bi]
	case a != nil && b != nil && c == nil:
		slice = slice[ai:bi]
	case a == nil && b != nil && c != nil:
		slice = slice[:bi:ci]
	case a != nil && b != nil && c != nil:
		slice = slice[ai:bi:ci]
	default:
		return nil, fmt.Errorf("invalid slice operation (%v, %v, %v)", a, b, c)
	}

	styp, ok := typ.(*sliceType)
	if !ok {
		styp = &sliceType{typ.Elem()}
	}

	return &sliceValue{styp, slice}, nil
}

func (t *arrayType) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	s, ok := typ.(*arrayType)
	if !ok || t.length != s.length {
		return false, nil
	}
	return t.elem.AssignableTo(ctx, s.elem)
}

func (t *sliceType) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	s, ok := typ.(*sliceType)
	if !ok {
		return false, nil
	}
	return t.elem.AssignableTo(ctx, s.elem)
}

func (t *arrayType) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	if v.Type() == t {
		return v, nil
	}
	panic("TODO")
}

func (t *sliceType) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	panic("TODO")
}

func (t *arrayType) NativeType(ctx vm.Context) (reflect.Type, bool) {
	typ, ok := common.NativeType(ctx, t.elem)
	if !ok {
		return nil, false
	}
	return reflect.ArrayOf(t.length, typ), true
}

func (t *sliceType) NativeType(ctx vm.Context) (reflect.Type, bool) {
	typ, ok := common.NativeType(ctx, t.elem)
	if !ok {
		return nil, false
	}
	return reflect.SliceOf(typ), true
}

func (v *arrayValue) NativeValue(ctx vm.Context) (reflect.Value, bool) {
	typ, ok := v.typ.NativeType(ctx)
	if !ok {
		return reflect.Value{}, false
	}
	u := reflect.New(typ).Elem()
	if !convertSlice(ctx, u, v.values) {
		return reflect.Value{}, false
	}
	return u, true
}

func (v *sliceValue) NativeValue(ctx vm.Context) (reflect.Value, bool) {
	typ, ok := v.typ.NativeType(ctx)
	if !ok {
		return reflect.Value{}, false
	}
	u := reflect.MakeSlice(typ, len(v.values), cap(v.values))
	if !convertSlice(ctx, u, v.values) {
		return reflect.Value{}, false
	}
	return u, true
}

func convertSlice(ctx vm.Context, u reflect.Value, values []vm.Value) bool {
	for i, v := range values {
		v, ok := common.NativeValue(ctx, v)
		if !ok {
			return false
		}
		u.Index(i).Set(v)
	}
	return true
}

func (o *OpMakeArrayType) Execute(ctx vm.Context) error {
	elem, err := popTypeOrIdent(ctx)
	if err != nil {
		return err
	}

	if o.Slice {
		ctx.Stack().Push(&sliceType{elem: elem})
		return nil
	}

	len, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return err
	}
	switch v := len.(type) {
	case vm.Int:
		ctx.Stack().Push(&arrayType{elem: elem, length: int(v.AsInt())})
	case vm.Uint:
		ctx.Stack().Push(&arrayType{elem: elem, length: int(v.AsUint())})
	default:
		return fmt.Errorf("cannot use %T as the length of an array", len)
	}
	return nil
}

func asInt(ptr *error, typ vm.Value, op string, index vm.Value) int {
	if index == nil {
		return 0
	}
	if *ptr != nil {
		return 0
	}
	switch v := index.(type) {
	case vm.Int:
		return int(v.AsInt())
	case vm.Uint:
		return int(v.AsUint())
	}
	*ptr = fmt.Errorf("cannot %s %v with %v", op, typ, index.Type())
	return 0
}
