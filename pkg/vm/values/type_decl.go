package values

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type typeDecl struct {
	name string
	typ  vm.Type
}

type OpMakeTypeDecl struct{}
type OpDeclareMember struct{}

var _ vm.Scope = (*typeDecl)(nil)

func (t *typeDecl) Kind() vm.DeclKind              { return vm.DeclType }
func (t *typeDecl) Name() string                   { return t.name }
func (t *typeDecl) Type() vm.Type                  { return common.TypeFor(t) }
func (t *typeDecl) Set(vm.Context, vm.Value) error { return vm.ErrImmutable }

func (t *typeDecl) Unwrap(ctx vm.Context) (vm.Value, error) {
	return t.typ, nil
}

func (t *typeDecl) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	return t.typ.AssignableTo(ctx, typ)
}

func (t *typeDecl) Convert(ctx vm.Context, value vm.Value, coerce bool) (vm.Value, error) {
	return t.typ.Convert(ctx, value, coerce)
}

func (t *typeDecl) New(ctx vm.Context) (vm.Value, error) {
	return t.typ.New(ctx)
}

func (t *typeDecl) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	typ, err := t.Unwrap(ctx)
	if err != nil {
		return nil, err
	}
	scope, ok := typ.(vm.Resolver)
	if !ok {
		return nil, common.SymbolNotFound(name)
	}
	return scope.Resolve(ctx, name)
}

func (t *typeDecl) Declare(ctx vm.Context, decl vm.Declaration) error {
	s, ok := t.typ.(vm.Scope)
	if !ok {
		return fmt.Errorf("%w: cannot declare a member on %v", vm.ErrWrongType, t.typ)
	}
	return s.Declare(ctx, decl)
}

func (*OpMakeTypeDecl) Execute(ctx vm.Context) error {
	name, err := common.PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	typ, err := common.PopUnwrap[vm.Type](ctx)
	if err != nil {
		return err
	}

	decl := new(typeDecl)
	decl.name = name
	decl.typ = typ
	ctx.Stack().Push(decl)
	return nil
}

func (o *OpDeclareMember) Execute(ctx vm.Context) error {
	typ, err := common.PopUnwrapEarly[vm.Scope](ctx)
	if err != nil {
		return err
	}

	decl, err := common.Pop[vm.Declaration](ctx)
	if err != nil {
		return err
	}
	if b, ok := decl.(vm.Bindable); ok {
		v, err := b.Bind(ctx)
		if err != nil {
			return err
		}
		decl = v.(vm.Declaration)
	}

	return typ.Declare(ctx, decl)
}
