package values

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type namedType struct {
	typ     vm.Type
	name    string
	methods map[string]*functionDecl
}

type OpMakeNamedType struct{}

func (t *namedType) Underlying() vm.Type { return t.typ }
func (t *namedType) String() string      { return t.name }

func (t *namedType) New(ctx vm.Context) (vm.Value, error) {
	var err error
	v := new(typedValue)
	v.typ = t
	v.val, err = t.typ.New(ctx)
	return v, err
}

func (t *namedType) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if fn, ok := t.methods[name]; ok {
		return fn, nil
	}
	return nil, common.SymbolNotFound(name)
}

func (t *namedType) Declare(ctx vm.Context, decl vm.Declaration) error {
	fn, ok := decl.(*functionDecl)
	if !ok {
		return fmt.Errorf("%T is not a valid member for a named type", decl)
	}
	if _, ok := t.methods[fn.name]; ok {
		return fmt.Errorf("invalid attempt to redefine %v", fn.name)
	}
	t.methods[fn.name] = fn
	return nil
}

func (t *namedType) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	return t == typ, nil
}

func (t *namedType) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	if !explicit {
		ok, err := v.Type().AssignableTo(ctx, t)
		if err != nil {
			return nil, err
		} else if !ok {
			return nil, common.CannotConvert(v.Type(), t)
		}
	}

	v, err := t.typ.Convert(ctx, v, true)
	if err != nil {
		return nil, err
	}

	return &typedValue{typ: t, val: v}, nil
}

func (o *OpMakeNamedType) Execute(ctx vm.Context) error {
	name, err := common.PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}

	typ, err := popTypeOrIdent(ctx)
	if err != nil {
		return err
	}

	ctx.Stack().Push(&namedType{name: name, typ: typ, methods: map[string]*functionDecl{}})
	return nil
}
