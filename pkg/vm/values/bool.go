package values

import (
	"fmt"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

type Bool struct{ V bool }
type boolType struct{}

func (v Bool) Type() vm.Type                                            { return boolType{} }
func (v Bool) NativeValue(vm.Context) (reflect.Value, bool)             { return rval(v) }
func (v Bool) String() string                                           { return fmt.Sprint(v.V) }
func (v Bool) AsBool() bool                                             { return v.V }
func (v Bool) Equal(u vm.Value) (bool, error)                           { return primEq(v, u) }
func (v Bool) Not() vm.Value                                            { return v.typ().new(!v.val()) }
func (t boolType) New(vm.Context) (vm.Value, error)                     { return Bool{}, nil }
func (t boolType) AssignableTo(_ vm.Context, typ vm.Type) (bool, error) { return typ == t, nil }
func (t boolType) NativeType() (reflect.Type, bool)                     { return rtyp[bool]() }

func (v Bool) val() bool                                 { return v.V }
func (v Bool) typ() boolType                             { return boolType{} }
func (t boolType) new(v bool) Bool                       { return Bool{v} }
func (t boolType) as() func(vm.Bool) bool                { return vm.Bool.AsBool }
func (t boolType) conv(v vm.Value, x bool) (bool, error) { return primConv(t, v, x) }
func (t boolType) fromUntyped(v bool) (bool, error)      { return v, nil }

func (t boolType) Convert(ctx vm.Context, v vm.Value, x bool) (vm.Value, error) {
	u, err := t.conv(v, x)
	return t.new(u), err
}
