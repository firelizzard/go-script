package values

import (
	"errors"
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type functionDecl struct {
	name string
	fn   *function
}

type function struct {
	name string
	sig  *FunctionSig
	bind vm.Scope
	body vm.Op
}

type boundFunctionDecl struct {
	decl vm.Declaration
	recv vm.Value
}

type boundFunction struct {
	fn   vm.Func
	recv vm.Value
}

type fnScope struct {
	fn    *function
	decls map[string]vm.Declaration
}

type OpMakeFuncDecl struct{}

type OpMakeFuncLit struct{}

var _ vm.Func = (*function)(nil)

func (f *functionDecl) Kind() vm.DeclKind                       { return vm.DeclFunc }
func (f *functionDecl) Name() string                            { return f.name }
func (f *functionDecl) Type() vm.Type                           { return common.TypeFor(f) }
func (f *functionDecl) Set(vm.Context, vm.Value) error          { return vm.ErrImmutable }
func (f *functionDecl) Unwrap(ctx vm.Context) (vm.Value, error) { return f.fn, nil }

func (f *function) Type() vm.Type { return f.sig }

func (f *function) Call(ctx vm.Context, in []vm.Value) ([]vm.Value, error) {
	if f.bind == nil {
		panic("attempted to evaluate unbound function")
	}
	if f.sig.variadic {
		panic("TODO")
	}

	// Validate number of inputs
	err := common.CheckValueCount(len(in), len(f.sig.in), f.sig.variadic)
	if err != nil {
		return nil, err
	}

	// Construct the function scope
	scope := &fnScope{fn: f, decls: map[string]vm.Declaration{}}
	err = scope.declare(ctx, f.sig.in, in)
	if err != nil {
		return nil, err
	}
	err = scope.declare(ctx, f.sig.out, nil)
	if err != nil {
		return nil, err
	}

	// Execute (in an isolated context)
	ctx2 := ctx.New()
	ctx2.Scope().Push(scope)

	err = f.body.Execute(ctx2)
	var ret common.ErrReturn
	switch {
	case err == nil:
		if len(f.sig.out) > 0 {
			return nil, errors.New("missing return statement")
		}
		return nil, nil

	case !errors.As(err, &ret):
		// Unknown error
		return nil, err

	case len(ret) > 0:
		// Explicit return, with values. Verify the number of values.
		if len(ret) != len(f.sig.out) {
			return nil, fmt.Errorf("not enough return values: have %d, want %d", len(ret), len(f.sig.out))
		}
		return ret, nil
	}

	// Explicit return, naked; push return values to the caller's stack
	var out []vm.Value
	for _, v := range f.sig.out {
		if v.name == "" || v.name == "_" {
			return nil, fmt.Errorf("not enough return values: have 0, want %d", len(f.sig.out))
		}
		u, err := common.Unwrap[vm.Value](ctx2, scope.decls[v.name])
		if err != nil {
			return nil, err
		}
		out = append(out, u)
	}
	return out, nil
}

func (f *boundFunctionDecl) Type() vm.Type                  { panic("TODO") }
func (f *boundFunctionDecl) Kind() vm.DeclKind              { return vm.DeclFunc }
func (f *boundFunctionDecl) Name() string                   { return f.decl.Name() }
func (f *boundFunctionDecl) Set(vm.Context, vm.Value) error { return vm.ErrImmutable }

func (f *boundFunctionDecl) Unwrap(ctx vm.Context) (vm.Value, error) {
	fn, err := common.Unwrap[vm.Func](ctx, f.decl)
	if err != nil {
		return nil, err
	}
	return &boundFunction{fn, f.recv}, nil
}

func (f *boundFunction) Type() vm.Type { panic("TODO") }

func (f *boundFunction) Call(ctx vm.Context, in []vm.Value) ([]vm.Value, error) {
	return f.fn.Call(ctx, append([]vm.Value{f.recv}, in...))
}

func (s *fnScope) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if d, ok := s.decls[name]; ok {
		return d, nil
	}
	return s.fn.bind.Resolve(ctx, name)
}

func (s *fnScope) Declare(_ vm.Context, decl vm.Declaration) error {
	name := decl.Name()
	if _, ok := s.decls[name]; ok {
		return fmt.Errorf("%v has already been declared", name)
	}
	s.decls[name] = decl
	return nil
}

func (s *fnScope) declare(ctx vm.Context, fields []*fieldType, args []vm.Value) error {
	for i, typ := range fields {
		if typ.name == "" || typ.name == "_" {
			continue
		}

		fld := &field{typ: typ}
		s.decls[typ.name] = fld
		if args == nil {
			continue
		}

		// TODO Type check
		val, err := common.Unwrap[vm.Value](ctx, args[i])
		if err != nil {
			return err
		}

		err = fld.Set(ctx, val)
		if err != nil {
			return err
		}
	}
	return nil
}

func (o *OpMakeFuncDecl) Execute(ctx vm.Context) error {
	name, err := common.PopUnwrapEarlyAs(ctx, vm.String.AsString)
	if err != nil {
		return err
	}
	sig, err := common.Pop[*FunctionSig](ctx)
	if err != nil {
		return err
	}
	body, err := common.Pop[vm.Op](ctx)
	if err != nil {
		return err
	}

	decl := new(functionDecl)
	decl.name = name
	decl.fn = new(function)
	decl.fn.name = name
	decl.fn.sig = sig
	decl.fn.body = body
	decl.fn.bind = ctx.Scope().Peek()
	ctx.Stack().Push(decl)
	return nil
}

func (o *OpMakeFuncLit) Execute(ctx vm.Context) error {
	sig, err := common.PopUnwrap[*FunctionSig](ctx)
	if err != nil {
		return err
	}
	body, err := common.PopUnwrap[vm.Op](ctx)
	if err != nil {
		return err
	}

	fn := new(function)
	fn.sig = sig
	fn.body = body
	fn.bind = ctx.Scope().Peek()
	ctx.Stack().Push(fn)
	return nil
}
