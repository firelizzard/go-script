package values

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
)

func ResolveType(name string) (vm.Type, bool) {
	// Types
	switch name {
	case "bool":
		return boolType{}, true
	case "int":
		return signedType[int]{}, true
	case "int8":
		return signedType[int8]{}, true
	case "int16":
		return signedType[int16]{}, true
	case "int32":
		return signedType[int32]{}, true
	case "int64":
		return signedType[int64]{}, true
	case "uint":
		return unsignedType[uint]{}, true
	case "uint8":
		return unsignedType[uint8]{}, true
	case "uint16":
		return unsignedType[uint16]{}, true
	case "uint32":
		return unsignedType[uint32]{}, true
	case "uint64":
		return unsignedType[uint64]{}, true
	case "uintptr":
		return unsignedType[uintptr]{}, true
	case "float32":
		return floatType[float32]{}, true
	case "float64":
		return floatType[float64]{}, true
	case "string":
		return stringType{}, true
	}

	return nil, false
}

func ResolveValue(name string) (vm.Value, bool) {
	switch name {
	case "true":
		return UntypedBool{V: true}, true
	case "false":
		return UntypedBool{V: false}, true
	}
	return nil, false
}
