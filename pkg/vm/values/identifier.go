package values

import (
	"errors"
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type Identifier struct{ V string }
type identifierType struct{}

func (v Identifier) Type() vm.Type                                            { return identifierType{} }
func (v Identifier) Kind() vm.DeclKind                                        { return vm.DeclVar }
func (v Identifier) Name() string                                             { return v.V }
func (v Identifier) String() string                                           { return v.V }
func (v Identifier) AsString() string                                         { return v.V }
func (v Identifier) Equal(u vm.Value) (bool, error)                           { return primEq(v, u) }
func (v Identifier) Add(u vm.Value) (vm.Value, error)                         { return primOp2(v, u, primAdd) }
func (t identifierType) String() string                                       { return "identifier" }
func (t identifierType) Type() vm.Type                                        { return common.TypeFor(t) }
func (t identifierType) New(vm.Context) (vm.Value, error)                     { return String{}, nil }
func (t identifierType) AssignableTo(_ vm.Context, typ vm.Type) (bool, error) { return typ == t, nil }

func (v Identifier) val() string                                 { return v.V }
func (v Identifier) typ() identifierType                         { return identifierType{} }
func (t identifierType) new(v string) Identifier                 { return Identifier{v} }
func (t identifierType) as() func(vm.String) string              { return vm.String.AsString }
func (t identifierType) conv(v vm.Value, x bool) (string, error) { return primConv(t, v, x) }
func (t identifierType) fromUntyped(v string) (string, error)    { return v, nil }

func (t identifierType) Convert(ctx vm.Context, v vm.Value, x bool) (vm.Value, error) {
	u, err := t.conv(v, x)
	return t.new(u), err
}

func (v Identifier) Unwrap(ctx vm.Context) (vm.Value, error) {
	return ctx.Scope().Peek().Resolve(ctx, v.V)
}

func (v Identifier) Set(ctx vm.Context, value vm.Value) error {
	decl, err := ctx.Scope().Peek().Resolve(ctx, v.V)
	if err != nil {
		return err
	}
	return decl.Set(ctx, value)
}

func (v Identifier) Bind(ctx vm.Context) (vm.Value, error) {
	u, err := ctx.Scope().Peek().Resolve(ctx, v.V)
	switch {
	case errors.Is(err, vm.ErrNotFound):
		return &boundIdentifier{v, nil, ctx.Scope().Peek()}, nil
	case err != nil:
		return nil, err
	default:
		return u, nil
	}
}

type boundIdentifier struct {
	Identifier
	value vm.Value
	scope vm.Scope
}

var _ vm.Type = (*boundIdentifier)(nil)

func (v *boundIdentifier) Unwrap(ctx vm.Context) (vm.Value, error) {
	if v.value != nil {
		return v.value, nil
	}
	u, err := v.scope.Resolve(ctx, v.V)
	if err != nil {
		return nil, err
	}
	v.value = u
	return u, nil
}

func (v *boundIdentifier) New(ctx vm.Context) (vm.Value, error) {
	typ, err := v.asType(ctx)
	if err != nil {
		return nil, err
	}
	return typ.New(ctx)
}

func (v *boundIdentifier) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := v.asType(ctx)
	if err != nil {
		return false, err
	}
	return typ.AssignableTo(ctx, typ)
}

func (v *boundIdentifier) Convert(ctx vm.Context, value vm.Value, coerce bool) (vm.Value, error) {
	typ, err := v.asType(ctx)
	if err != nil {
		return nil, err
	}
	return typ.Convert(ctx, value, coerce)
}

func (v *boundIdentifier) asType(ctx vm.Context) (vm.Type, error) {
	typ, err := common.Unwrap[vm.Type](ctx, v)
	switch {
	case err == nil:
		return typ, nil
	case !errors.Is(err, vm.ErrWrongType):
		return nil, err
	default:
		return nil, fmt.Errorf("%w: %v is not a type", vm.ErrWrongType, v.V)
	}
}
