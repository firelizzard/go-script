package values

import (
	"errors"
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type typedValue struct {
	typ vm.Type
	val vm.Value
}

func (v *typedValue) Type() vm.Type        { return v.typ }
func (v *typedValue) String() string       { return fmt.Sprint(v.val) }
func (t *typedValue) Underlying() vm.Value { return t.val }

func (v *typedValue) ConvertTo(ctx vm.Context, typ vm.Type, coerce bool) (vm.Value, error) {
	if v.typ == typ {
		return v, nil
	}
	return typ.Convert(ctx, v, coerce)
}

func (v *typedValue) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if r, ok := v.val.(vm.Resolver); ok {
		u, err := r.Resolve(ctx, name)
		if !errors.Is(err, vm.ErrNotFound) {
			return u, err
		}
	}
	if r, ok := v.typ.(vm.Resolver); ok {
		u, err := r.Resolve(ctx, name)
		switch {
		case errors.Is(err, vm.ErrNotFound):
			break
		case err != nil:
			return nil, err
		default:
			decl, ok := u.(vm.Declaration)
			if !ok || decl.Kind() != vm.DeclFunc {
				return u, nil
			}
			return &boundFunctionDecl{decl, v}, nil
		}
	}
	return nil, common.NotAMember(name, v.typ)
}
