package values

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type structValue struct {
	typ    *structType
	fields []*field
}

type structType struct {
	fields []*fieldType
	goType reflect.Type
}

type OpMakeStructType struct {
	Fields int
}

func (v *structValue) Type() vm.Type { return v.typ }

func (t *structType) New(ctx vm.Context) (vm.Value, error) {
	v := new(structValue)
	v.typ = t
	v.fields = make([]*field, len(t.fields))
	for i, f := range t.fields {
		u, err := f.typ.New(ctx)
		if err != nil {
			return nil, err
		}
		v.fields[i] = &field{typ: f, val: u}
	}
	return v, nil
}

func (t *structType) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	return t == typ, nil
}

func (t *structType) Convert(ctx vm.Context, v vm.Value, explicit bool) (vm.Value, error) {
	if explicit {
		v = common.Underlying(v)
	}

	u, ok := v.(*structValue)
	if !ok || !explicit && u.typ != t || explicit && !u.typ.equal(t) {
		return nil, common.CannotConvert(v.Type(), t)
	}

	if u.typ == t {
		return u, nil
	}

	w := &structValue{typ: t, fields: make([]*field, len(t.fields))}
	for i, u := range u.fields {
		w.fields[i] = &field{typ: t.fields[i], val: u.val}
	}
	return w, nil
}

func (t *structType) equal(s *structType) bool {
	if len(t.fields) != len(s.fields) {
		return false
	}
	for i := range t.fields {
		if !t.fields[i].equal(s.fields[i]) {
			return false
		}
	}
	return true
}

func (v *structValue) Resolve(_ vm.Context, name string) (vm.Declaration, error) {
	for _, f := range v.fields {
		if f.Name() == name {
			return f, nil
		}
	}
	return nil, common.NotAMember(name, v.typ)
}

func (o *OpMakeStructType) Execute(ctx vm.Context) error {
	typ := new(structType)
	typ.fields = make([]*fieldType, o.Fields)
	for i, field := range ctx.Stack().PopN(o.Fields) {
		var err error
		typ.fields[i], err = common.Unwrap[*fieldType](ctx, field)
		if err != nil {
			return err
		}
	}

	ctx.Stack().Push(typ)
	return nil
}
