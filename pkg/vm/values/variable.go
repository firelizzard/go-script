package values

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type variable struct {
	name      string
	typ       vm.Type
	val       vm.Value
	immutable bool
}

type OpMakeVariable struct {
	IsConst bool
}

func (v *variable) Name() string                        { return v.name }
func (v *variable) Type() vm.Type                       { return v.typ }
func (v *variable) Unwrap(vm.Context) (vm.Value, error) { return v.val, nil }

func (v *variable) AsConst() vm.Declaration {
	u := *v
	u.immutable = true
	return &u
}

func (v *variable) Kind() vm.DeclKind {
	if v.immutable {
		return vm.DeclConst
	}
	return vm.DeclVar
}

func (v *variable) Set(ctx vm.Context, u vm.Value) error {
	if v.immutable {
		return vm.ErrImmutable
	}

	u, err := v.typ.Convert(ctx, u, false)
	if err != nil {
		return err
	}

	v.val = u
	return nil
}

func (o *OpMakeVariable) Execute(ctx vm.Context) error {
	typ, err := popExecTypeOrIdent(ctx)
	if err != nil {
		return err
	}

	names, err := popExecAs[vm.String](ctx, true)
	if err != nil {
		return err
	}

	values, err := popExecAs[vm.Value](ctx, false)
	if err != nil {
		return err
	}

	if o.IsConst && len(values) == 0 {
		panic("cannot define a constant without a value")
	}
	if typ == nil && len(values) == 0 {
		panic("cannot define a variable without a type and/or a value")
	}

	err = common.CheckValueCount(len(values), len(names), false)
	if err != nil {
		return err
	}

	for i, name := range names {
		v := &variable{
			name:      name.AsString(),
			immutable: o.IsConst,
		}

		typ := typ
		if typ == nil {
			typ = values[i].Type()
			if u, ok := typ.(vm.Untype); ok {
				typ = u.Concrete()
			}
		}
		v.typ = typ

		if len(values) > 0 {
			err = v.Set(ctx, values[i])
			if err != nil {
				return err
			}
		}
		ctx.Stack().Push(v)
	}

	return nil
}
