package values

import (
	"fmt"
	"math/big"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"golang.org/x/exp/constraints"
)

type Int = signedValue[int]
type Int8 = signedValue[int8]
type Int16 = signedValue[int16]
type Int32 = signedValue[int32]
type Int64 = signedValue[int64]
type Uint = unsignedValue[uint]
type Uint8 = unsignedValue[uint8]
type Uint16 = unsignedValue[uint16]
type Uint32 = unsignedValue[uint32]
type Uint64 = unsignedValue[uint64]
type Uintptr = unsignedValue[uintptr]
type Float32 = floatValue[float32]
type Float64 = floatValue[float64]

type signedValue[V constraints.Signed] struct{ V V }
type unsignedValue[V constraints.Unsigned] struct{ V V }
type floatValue[V constraints.Float] struct{ V V }

type signedType[V constraints.Signed] struct{}
type unsignedType[V constraints.Unsigned] struct{}
type floatType[V constraints.Float] struct{}

func (signedValue[V]) Type() vm.Type   { return signedType[V]{} }
func (unsignedValue[V]) Type() vm.Type { return unsignedType[V]{} }
func (floatValue[V]) Type() vm.Type    { return floatType[V]{} }

func (v signedType[V]) NativeType(vm.Context) (reflect.Type, bool)   { return rtyp[V]() }
func (v unsignedType[V]) NativeType(vm.Context) (reflect.Type, bool) { return rtyp[V]() }
func (v floatType[V]) NativeType(vm.Context) (reflect.Type, bool)    { return rtyp[V]() }

func (v signedValue[V]) NativeValue(vm.Context) (reflect.Value, bool)   { return rval(v.V) }
func (v unsignedValue[V]) NativeValue(vm.Context) (reflect.Value, bool) { return rval(v.V) }
func (v floatValue[V]) NativeValue(vm.Context) (reflect.Value, bool)    { return rval(v.V) }

func (t signedType[V]) AssignableTo(_ vm.Context, typ vm.Type) (bool, error)   { return typ == t, nil }
func (t unsignedType[V]) AssignableTo(_ vm.Context, typ vm.Type) (bool, error) { return typ == t, nil }
func (t floatType[V]) AssignableTo(_ vm.Context, typ vm.Type) (bool, error)    { return typ == t, nil }

func (t signedType[V]) New(vm.Context) (vm.Value, error)   { return signedValue[V]{}, nil }
func (t unsignedType[V]) New(vm.Context) (vm.Value, error) { return unsignedValue[V]{}, nil }
func (t floatType[V]) New(vm.Context) (vm.Value, error)    { return floatValue[V]{}, nil }

func (t signedType[V]) String() string   { return reflect.TypeFor[V]().String() }
func (t unsignedType[V]) String() string { return reflect.TypeFor[V]().String() }
func (t floatType[V]) String() string    { return reflect.TypeFor[V]().String() }

func (v signedValue[V]) String() string   { return fmt.Sprint(v.V) }
func (v unsignedValue[V]) String() string { return fmt.Sprint(v.V) }
func (v floatValue[V]) String() string    { return fmt.Sprint(v.V) }

func (v signedValue[V]) AsInt() int64     { return int64(v.V) }
func (v unsignedValue[V]) AsUint() uint64 { return uint64(v.V) }
func (v floatValue[V]) AsFloat() float64  { return float64(v.V) }

func (v signedValue[V]) Neg() vm.Value   { return v.typ().new(-v.val()) }
func (v unsignedValue[V]) Neg() vm.Value { return v.typ().new(-v.val()) }
func (v floatValue[V]) Neg() vm.Value    { return v.typ().new(-v.val()) }

func (v signedValue[V]) Inv() vm.Value   { return v.typ().new(^v.val()) }
func (v unsignedValue[V]) Inv() vm.Value { return v.typ().new(^v.val()) }

func (v signedValue[V]) Equal(u vm.Value) (bool, error)   { return primEq(v, u) }
func (v unsignedValue[V]) Equal(u vm.Value) (bool, error) { return primEq(v, u) }
func (v floatValue[V]) Equal(u vm.Value) (bool, error)    { return primEq(v, u) }

func (v signedValue[V]) Add(u vm.Value) (vm.Value, error)   { return primOp2(v, u, primAdd) }
func (v unsignedValue[V]) Add(u vm.Value) (vm.Value, error) { return primOp2(v, u, primAdd) }
func (v floatValue[V]) Add(u vm.Value) (vm.Value, error)    { return primOp2(v, u, primAdd) }

func (v signedValue[V]) Sub(u vm.Value) (vm.Value, error)   { return primOp2(v, u, primSub) }
func (v unsignedValue[V]) Sub(u vm.Value) (vm.Value, error) { return primOp2(v, u, primSub) }
func (v floatValue[V]) Sub(u vm.Value) (vm.Value, error)    { return primOp2(v, u, primSub) }

func (v signedValue[V]) Mul(u vm.Value) (vm.Value, error)   { return primOp2(v, u, primMul) }
func (v unsignedValue[V]) Mul(u vm.Value) (vm.Value, error) { return primOp2(v, u, primMul) }
func (v floatValue[V]) Mul(u vm.Value) (vm.Value, error)    { return primOp2(v, u, primMul) }

func (v signedValue[V]) Div(u vm.Value) (vm.Value, error)   { return primOp2(v, u, primDiv) }
func (v unsignedValue[V]) Div(u vm.Value) (vm.Value, error) { return primOp2(v, u, primDiv) }
func (v floatValue[V]) Div(u vm.Value) (vm.Value, error)    { return primOp2(v, u, primDiv) }

func (v signedValue[V]) LessThan(u vm.Value) (bool, error)   { return primLess(v, u) }
func (v unsignedValue[V]) LessThan(u vm.Value) (bool, error) { return primLess(v, u) }
func (v floatValue[V]) LessThan(u vm.Value) (bool, error)    { return primLess(v, u) }

func (v signedValue[V]) GreaterThan(u vm.Value) (bool, error)   { return primGreater(v, u) }
func (v unsignedValue[V]) GreaterThan(u vm.Value) (bool, error) { return primGreater(v, u) }
func (v floatValue[V]) GreaterThan(u vm.Value) (bool, error)    { return primGreater(v, u) }

func (v signedValue[V]) val() V   { return v.V }
func (v unsignedValue[V]) val() V { return v.V }
func (v floatValue[V]) val() V    { return v.V }

func (signedValue[V]) typ() signedType[V]     { return signedType[V]{} }
func (unsignedValue[V]) typ() unsignedType[V] { return unsignedType[V]{} }
func (floatValue[V]) typ() floatType[V]       { return floatType[V]{} }

func (t signedType[V]) conv(v vm.Value, x bool) (V, error)   { return numConv(t, v, x) }
func (t unsignedType[V]) conv(v vm.Value, x bool) (V, error) { return numConv(t, v, x) }
func (t floatType[V]) conv(v vm.Value, x bool) (V, error)    { return numConv(t, v, x) }

func (t signedType[V]) new(v V) signedValue[V]     { return signedValue[V]{v} }
func (t unsignedType[V]) new(v V) unsignedValue[V] { return unsignedValue[V]{v} }
func (t floatType[V]) new(v V) floatValue[V]       { return floatValue[V]{v} }

func (t signedType[V]) fromUntyped(v *big.Float) (V, error) {
	var z V
	u, acc := v.Int(nil)
	if acc != big.Exact {
		return z, fmt.Errorf("cannot use %v as %v value (truncated)", v, t)
	}

	// U will fit within a signed n-bit integer if and only if U reports a bit
	// length of n-1, **except** in one case: U = -2^(n-1). In that one case, U
	// will report a bit length of n, but it still fits into a signed n-bit
	// integer. So, if the value is negative we add 1 before checking the bit
	// length.
	if u.Sign() < 0 {
		u.Add(u, big.NewInt(1))
	}
	if bitLen[V]() <= u.BitLen() {
		return z, fmt.Errorf("cannot use %v as %v value (overflows)", v, t)
	}

	return V(u.Int64()), nil
}

func (t unsignedType[V]) fromUntyped(v *big.Float) (V, error) {
	var z V
	u, acc := v.Int(nil)
	if acc != big.Exact {
		return z, fmt.Errorf("cannot use %v as %v value (truncated)", v, t)
	}
	if u.Sign() < 0 || bitLen[V]() < u.BitLen() {
		return z, fmt.Errorf("cannot use %v as %v value (overflows)", v, t)
	}
	return V(u.Uint64()), nil
}

func (t floatType[V]) fromUntyped(v *big.Float) (V, error) {
	// Go doesn't care if float constants overflow/truncate, so we won't care
	// either
	u, _ := v.Float64()
	return V(u), nil
}

func (t signedType[V]) Convert(_ vm.Context, v vm.Value, x bool) (vm.Value, error) {
	u, err := t.conv(v, x)
	return t.new(u), err
}

func (t unsignedType[V]) Convert(_ vm.Context, v vm.Value, x bool) (vm.Value, error) {
	u, err := t.conv(v, x)
	return t.new(u), err
}

func (t floatType[V]) Convert(_ vm.Context, v vm.Value, x bool) (vm.Value, error) {
	u, err := t.conv(v, x)
	return t.new(u), err
}

func numConv[A numeric, B primVal[A, C], C primNumTyp[A, B, C, D], D any](typ C, v vm.Value, explicit bool) (A, error) {
	for {
		switch v := v.(type) {
		case vm.Untyped[D]:
			return typ.fromUntyped(v.AsUntyped())

		case vm.Int:
			if explicit {
				return A(v.AsInt()), nil
			}
			if u, ok := v.(B); ok {
				return u.val(), nil
			}
		case vm.Uint:
			if explicit {
				return A(v.AsUint()), nil
			}
			if u, ok := v.(B); ok {
				return u.val(), nil
			}
		case vm.Float:
			if explicit {
				return A(v.AsFloat()), nil
			}
			if u, ok := v.(B); ok {
				return u.val(), nil
			}
		}

		if u, ok := v.(vm.Named); ok && explicit {
			v = u.Underlying()
			continue
		}

		return 0, common.CannotConvert(v.Type(), typ)
	}
}

func bitLen[V numeric]() int {
	var z V
	switch any(z).(type) {
	case int, uint:
		return 32 << (^uint(0) >> 63) // intSize from "math"
	case int8, uint8:
		return 8
	case int16, uint16:
		return 16
	case int32, uint32, float32:
		return 32
	case int64, uint64, float64:
		return 64
	}
	panic("not an numeric type")
}
