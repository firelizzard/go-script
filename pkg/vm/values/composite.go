package values

import (
	"fmt"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type OpMakeComposite struct {
	NumArgs int
}

func (o *OpMakeComposite) Execute(ctx vm.Context) error {
	args := ctx.Stack().PopN(o.NumArgs)
	typ, err := common.PopUnwrap[vm.Type](ctx)
	if err != nil {
		return err
	}

	val, err := o.execute(ctx, args, typ)
	if err != nil {
		return err
	}

	ctx.Stack().Push(val)
	return nil
}

func (o *OpMakeComposite) execute(ctx vm.Context, args []vm.Value, typ vm.Type) (vm.Value, error) {
	switch typ := typ.(type) {
	case *namedType:
		v, err := o.execute(ctx, args, typ.typ)
		if err != nil {
			return nil, err
		}
		return &typedValue{typ: typ, val: v}, nil

	case *structType:
		if len(args) != len(typ.fields) {
			return nil, fmt.Errorf("wrong number of values: want %v, got %v", len(typ.fields), len(args))
		}
		val := new(structValue)
		val.typ = typ
		val.fields = make([]*field, len(args))
		for i := range args {
			fld := &field{typ: typ.fields[i]}
			val.fields[i] = fld

			arg, err := common.Unwrap[vm.Value](ctx, args[i])
			if err != nil {
				return nil, err
			}
			err = fld.Set(ctx, arg)
			if err != nil {
				return nil, err
			}
		}
		return val, nil

	case *arrayType:
		if typ.length >= 0 && len(args) > typ.length {
			return nil, fmt.Errorf("wrong number of values: want %v, got %v", typ.length, len(args))
		}

		val := &arrayValue{typ: typ}
		val.values = make([]vm.Value, typ.length)
		for i, arg := range args {
			v, err := typ.elem.Convert(ctx, arg, false)
			if err != nil {
				return nil, err
			}
			val.values[i] = v
		}

		var err error
		for i := len(args); i < typ.length; i++ {
			val.values[i], err = typ.elem.New(ctx)
			if err != nil {
				return nil, err
			}
		}
		return val, nil

	case *sliceType:
		val := new(sliceValue)
		val.typ = typ
		val.values = make([]vm.Value, len(args))
		for i, arg := range args {
			v, err := typ.elem.Convert(ctx, arg, false)
			if err != nil {
				return nil, err
			}
			val.values[i] = v
		}
		return val, nil

	default:
		return nil, fmt.Errorf("composite literal: unsupported type %T", typ)
	}
}
