package values

import (
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
)

type String struct{ V string }
type stringType struct{}

func (v String) Type() vm.Type                                            { return stringType{} }
func (v String) String() string                                           { return v.V }
func (v String) AsString() string                                         { return v.V }
func (v String) Equal(u vm.Value) (bool, error)                           { return primEq(v, u) }
func (v String) Add(u vm.Value) (vm.Value, error)                         { return primOp2(v, u, primAdd) }
func (v String) NativeValue(vm.Context) (reflect.Value, bool)             { return rval(v.V) }
func (t stringType) NativeType(vm.Context) (reflect.Type, bool)           { return rtyp[string]() }
func (t stringType) String() string                                       { return "string" }
func (t stringType) New(vm.Context) (vm.Value, error)                     { return String{}, nil }
func (t stringType) AssignableTo(_ vm.Context, typ vm.Type) (bool, error) { return typ == t, nil }

func (v String) val() string                                 { return v.V }
func (v String) typ() stringType                             { return stringType{} }
func (t stringType) new(v string) String                     { return String{v} }
func (t stringType) as() func(vm.String) string              { return vm.String.AsString }
func (t stringType) conv(v vm.Value, x bool) (string, error) { return primConv(t, v, x) }
func (t stringType) fromUntyped(v string) (string, error)    { return v, nil }

func (t stringType) Convert(_ vm.Context, v vm.Value, x bool) (vm.Value, error) {
	u, err := t.conv(v, x)
	return t.new(u), err
}
