package values

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

var (
	_ vm.Op = (*OpDeclareMember)(nil)
	_ vm.Op = (*OpMakeArrayType)(nil)
	_ vm.Op = (*OpMakeComposite)(nil)
	_ vm.Op = (*OpMakeFieldType)(nil)
	_ vm.Op = (*OpMakeFuncDecl)(nil)
	_ vm.Op = (*OpMakeFuncLit)(nil)
	_ vm.Op = (*OpMakeFuncSig)(nil)
	_ vm.Op = (*OpMakeNamedType)(nil)
	_ vm.Op = (*OpMakeStructType)(nil)
	_ vm.Op = (*OpMakeTypeDecl)(nil)
	_ vm.Op = (*OpMakeVariable)(nil)
)

func (o *OpDeclareMember) Type() vm.Type  { return common.TypeFor(o) }
func (o *OpMakeArrayType) Type() vm.Type  { return common.TypeFor(o) }
func (o *OpMakeComposite) Type() vm.Type  { return common.TypeFor(o) }
func (o *OpMakeFieldType) Type() vm.Type  { return common.TypeFor(o) }
func (o *OpMakeFuncDecl) Type() vm.Type   { return common.TypeFor(o) }
func (o *OpMakeFuncLit) Type() vm.Type    { return common.TypeFor(o) }
func (o *OpMakeFuncSig) Type() vm.Type    { return common.TypeFor(o) }
func (o *OpMakeNamedType) Type() vm.Type  { return common.TypeFor(o) }
func (o *OpMakeStructType) Type() vm.Type { return common.TypeFor(o) }
func (o *OpMakeTypeDecl) Type() vm.Type   { return common.TypeFor(o) }
func (o *OpMakeVariable) Type() vm.Type   { return common.TypeFor(o) }

var (
	_ vm.Type  = (*arrayType)(nil)
	_ vm.Type  = (*boolType)(nil)
	_ vm.Value = (*fieldType)(nil) // fieldType is just a Value, not a Type
	_ vm.Type  = (*floatType[float64])(nil)
	_ vm.Type  = (*namedType)(nil)
	_ vm.Type  = (*signedType[int])(nil)
	_ vm.Type  = (*sliceType)(nil)
	_ vm.Type  = (*stringType)(nil)
	_ vm.Type  = (*structType)(nil)
	_ vm.Type  = (*unsignedType[uint])(nil)
)

func (t arrayType) Type() vm.Type          { return common.TypeFor(t) }
func (t boolType) Type() vm.Type           { return common.TypeFor(t) }
func (t fieldType) Type() vm.Type          { return common.TypeFor(t) }
func (t floatType[float64]) Type() vm.Type { return common.TypeFor(t) }
func (t namedType) Type() vm.Type          { return common.TypeFor(t) }
func (t signedType[int]) Type() vm.Type    { return common.TypeFor(t) }
func (t sliceType) Type() vm.Type          { return common.TypeFor(t) }
func (t stringType) Type() vm.Type         { return common.TypeFor(t) }
func (t structType) Type() vm.Type         { return common.TypeFor(t) }
func (t unsignedType[uint]) Type() vm.Type { return common.TypeFor(t) }
