package values

import (
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type interfaceValue struct {
	typ *interfaceType
	val vm.Value
}

type interfaceType struct {
	methods map[string]*FunctionSig
}

var _ vm.Type = (*interfaceType)(nil)

func (v *interfaceValue) Type() vm.Type { return v.typ }

func (t interfaceType) Type() vm.Type { return common.TypeFor(t) }

func (t *interfaceType) Convert(v vm.Value, explicit bool) (vm.Value, error) {
	panic("TODO")
}
