package values

import (
	"fmt"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

func popTypeOrIdent(ctx vm.Context) (vm.Type, error) {
	return typeOrIdent(ctx, ctx.Stack().Pop())
}

func typeOrIdent(ctx vm.Context, v vm.Value) (vm.Type, error) {
	for {
		switch u := v.(type) {
		case vm.Type:
			return u, nil

		case Identifier:
			w, err := u.Bind(ctx)
			if err != nil {
				return nil, err
			}
			v = w

		default:
			return nil, common.WrongType(reflect.TypeFor[vm.Type](), reflect.TypeOf(u))
		}
	}
}

func popExecAs[V any](ctx vm.Context, early bool) ([]V, error) {
	raw, err := common.PopAndExecuteInFrame(ctx)
	if err != nil {
		return nil, err
	}

	// Pop all values
	values := make([]V, len(raw))
	for i, value := range raw {
		if early {
			values[i], err = common.UnwrapEarly[V](ctx, value)
		} else {
			values[i], err = common.Unwrap[V](ctx, value)
		}
		if err != nil {
			return nil, err
		}
	}
	return values, nil
}

func popExecTypeOrIdent(ctx vm.Context) (vm.Type, error) {
	raw, err := common.PopAndExecuteInFrame(ctx)
	if err != nil {
		return nil, err
	}
	if len(raw) == 0 {
		return nil, nil
	}
	if len(raw) > 1 {
		return nil, fmt.Errorf("expected 1 type, got %d", len(raw))
	}

	typ, err := typeOrIdent(ctx, raw[0])
	if err != nil {
		return nil, err
	}
	return typ, nil
}
