package values

import (
	"errors"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"golang.org/x/exp/constraints"
)

type integer = constraints.Integer

type numeric = interface {
	constraints.Integer | constraints.Float
}

type primVal[V comparable, T any] interface {
	vm.Value
	typ() T
	val() V
}

type primTyp[U comparable, V primVal[U, T], T any] interface {
	new(U) V
	conv(v vm.Value, x bool) (U, error)
}

type primTyp2[A comparable, B primVal[A, C], C, D, E any] interface {
	primTyp[A, B, C]
	as() func(D) A
	fromUntyped(E) (A, error)
}

type primNumTyp[A comparable, B primVal[A, C], C, D any] interface {
	primTyp[A, B, C]
	fromUntyped(D) (A, error)
}

func primConv[A vm.Value, B comparable, C primVal[B, D], D primTyp2[B, C, D, A, E], E any](typ D, v vm.Value, explicit bool) (B, error) {
	for {
		if u, ok := v.(vm.Untyped[E]); ok {
			return typ.fromUntyped(u.AsUntyped())
		}
		if u, ok := v.(C); !explicit && ok {
			return u.val(), nil
		} else if u, ok := v.(A); explicit && ok {
			return typ.as()(u), nil
		}

		if u, ok := v.(vm.Named); ok && explicit {
			v = u.Underlying()
			continue
		}

		var z B
		return z, common.CannotConvert(v.Type(), typ)
	}
}

func primOp2[A comparable, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value, op func(B, vm.Value) (A, error)) (B, error) {
	w, err := op(v, u)
	return v.typ().new(w), err
}

func primEq[A comparable, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (bool, error) {
	u2, err := v.typ().conv(u, false)
	switch {
	case errors.Is(err, vm.ErrCannotConvert):
		return false, nil
	case err != nil:
		return false, err
	}

	return v.val() == u2, nil
}

func primAdd[A numeric | string, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (A, error) {
	u2, err := v.typ().conv(u, false)
	if err != nil {
		var z A
		return z, err
	}
	return v.val() + u2, nil
}

func primSub[A numeric, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (A, error) {
	u2, err := v.typ().conv(u, false)
	if err != nil {
		return 0, err
	}
	return v.val() - u2, nil
}

func primMul[A numeric, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (A, error) {
	u2, err := v.typ().conv(u, false)
	if err != nil {
		return 0, err
	}
	return v.val() * u2, nil
}

func primDiv[A numeric, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (A, error) {
	u2, err := v.typ().conv(u, false)
	if err != nil {
		return 0, err
	}
	return v.val() / u2, nil
}

func primLess[A numeric, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (bool, error) {
	u2, err := v.typ().conv(u, false)
	if err != nil {
		return false, err
	}

	return v.val() < u2, nil
}

func primGreater[A numeric, B primVal[A, C], C primTyp[A, B, C]](v B, u vm.Value) (bool, error) {
	u2, err := v.typ().conv(u, false)
	if err != nil {
		return false, err
	}

	return v.val() > u2, nil
}

func rtyp[V any]() (reflect.Type, bool) {
	return reflect.TypeFor[V](), true
}

func rval(v any) (reflect.Value, bool) {
	return reflect.ValueOf(v), true
}
