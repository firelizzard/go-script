package values

import (
	"math/big"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
)

type UntypedBool struct{ V bool }
type UntypedInt struct{ V big.Int }
type UntypedFloat struct{ V big.Float }
type UntypedString struct{ V string }

type boolUntype struct{}
type intUntype struct{}
type floatUntype struct{}
type stringUntype struct{}

var _ vm.Untyped[bool] = (*UntypedBool)(nil)
var _ vm.Untyped[*big.Float] = (*UntypedInt)(nil)
var _ vm.Untyped[*big.Float] = (*UntypedFloat)(nil)
var _ vm.Untyped[string] = (*UntypedString)(nil)

func (UntypedBool) Type() vm.Type   { return boolUntype{} }
func (*UntypedInt) Type() vm.Type   { return intUntype{} }
func (*UntypedFloat) Type() vm.Type { return floatUntype{} }
func (UntypedString) Type() vm.Type { return stringUntype{} }

func (v UntypedBool) NativeValue(vm.Context) (reflect.Value, bool)   { return rval(v.V) }
func (v *UntypedInt) NativeValue(vm.Context) (reflect.Value, bool)   { return rval(v.V) }
func (v *UntypedFloat) NativeValue(vm.Context) (reflect.Value, bool) { return rval(v.V) }
func (v UntypedString) NativeValue(vm.Context) (reflect.Value, bool) { return rval(v.V) }

func (v UntypedBool) AsBool() bool            { return v.V }
func (v UntypedBool) AsUntyped() bool         { return v.V }
func (v *UntypedInt) AsInt() int64            { return v.V.Int64() }
func (v *UntypedInt) AsUint() uint64          { return v.V.Uint64() }
func (v *UntypedInt) AsFloat() float64        { u, _ := v.V.Float64(); return u }
func (v *UntypedInt) AsUntyped() *big.Float   { u := new(big.Float); return u.SetInt(&v.V) }
func (v *UntypedFloat) AsInt() int64          { u, _ := v.V.Int64(); return u }
func (v *UntypedFloat) AsUint() uint64        { u, _ := v.V.Uint64(); return u }
func (v *UntypedFloat) AsFloat() float64      { u, _ := v.V.Float64(); return u }
func (v *UntypedFloat) AsUntyped() *big.Float { return &v.V }
func (v UntypedString) AsString() string      { return v.V }
func (v UntypedString) AsUntyped() string     { return v.V }

func (v UntypedBool) Equal(u vm.Value) (bool, error)   { return primEq(v, u) }
func (v *UntypedInt) Equal(u vm.Value) (bool, error)   { return bigCmp(v, u, 0) }
func (v *UntypedFloat) Equal(u vm.Value) (bool, error) { return bigCmp(v, u, 0) }
func (v UntypedString) Equal(u vm.Value) (bool, error) { return primEq(v, u) }

func (v *UntypedInt) Add(u vm.Value) (vm.Value, error)   { return bigOp(v, u, (*big.Int).Add) }
func (v *UntypedFloat) Add(u vm.Value) (vm.Value, error) { return bigOp(v, u, (*big.Float).Add) }
func (v UntypedString) Add(u vm.Value) (vm.Value, error) { return primOp2(v, u, primAdd) }

func (v *UntypedInt) Sub(u vm.Value) (vm.Value, error)   { return bigOp(v, u, (*big.Int).Sub) }
func (v *UntypedFloat) Sub(u vm.Value) (vm.Value, error) { return bigOp(v, u, (*big.Float).Sub) }

func (v *UntypedInt) Mul(u vm.Value) (vm.Value, error)   { return bigOp(v, u, (*big.Int).Mul) }
func (v *UntypedFloat) Mul(u vm.Value) (vm.Value, error) { return bigOp(v, u, (*big.Float).Mul) }

func (v *UntypedInt) Div(u vm.Value) (vm.Value, error)   { return bigOp(v, u, (*big.Int).Div) }
func (v *UntypedFloat) Div(u vm.Value) (vm.Value, error) { return bigOp(v, u, (*big.Float).Quo) }

func (v *UntypedInt) LessThan(u vm.Value) (bool, error)   { return bigCmp(v, u, -1) }
func (v *UntypedFloat) LessThan(u vm.Value) (bool, error) { return bigCmp(v, u, -1) }

func (v *UntypedInt) GreaterThan(u vm.Value) (bool, error)   { return bigCmp(v, u, +1) }
func (v *UntypedFloat) GreaterThan(u vm.Value) (bool, error) { return bigCmp(v, u, +1) }

func (t boolUntype) String() string   { return "untyped bool" }
func (t intUntype) String() string    { return "untyped int" }
func (t floatUntype) String() string  { return "untyped float" }
func (t stringUntype) String() string { return "untyped string" }

func (t boolUntype) Type() vm.Type   { return common.TypeFor(t) }
func (t intUntype) Type() vm.Type    { return common.TypeFor(t) }
func (t floatUntype) Type() vm.Type  { return common.TypeFor(t) }
func (t stringUntype) Type() vm.Type { return common.TypeFor(t) }

func (t boolUntype) Concrete() vm.Type   { return boolType{} }
func (t intUntype) Concrete() vm.Type    { return signedType[int]{} }
func (t floatUntype) Concrete() vm.Type  { return floatType[float64]{} }
func (t stringUntype) Concrete() vm.Type { return stringType{} }

func (t boolUntype) New(vm.Context) (vm.Value, error)   { return UntypedBool{}, nil }
func (t intUntype) New(vm.Context) (vm.Value, error)    { return &UntypedInt{}, nil }
func (t floatUntype) New(vm.Context) (vm.Value, error)  { return &UntypedFloat{}, nil }
func (t stringUntype) New(vm.Context) (vm.Value, error) { return UntypedString{}, nil }

func (UntypedBool) typ() boolUntype     { return boolUntype{} }
func (*UntypedInt) typ() intUntype      { return intUntype{} }
func (*UntypedFloat) typ() floatUntype  { return floatUntype{} }
func (UntypedString) typ() stringUntype { return stringUntype{} }

func (v UntypedBool) val() bool         { return v.V }
func (v *UntypedInt) val() *big.Int     { return &v.V }
func (v *UntypedFloat) val() *big.Float { return &v.V }
func (v UntypedString) val() string     { return v.V }

func (boolUntype) new(v bool) UntypedBool          { return UntypedBool{V: v} }
func (intUntype) new(v *big.Int) *UntypedInt       { return &UntypedInt{V: *v} }
func (floatUntype) new(v *big.Float) *UntypedFloat { return &UntypedFloat{V: *v} }
func (stringUntype) new(v string) UntypedString    { return UntypedString{V: v} }

func (boolUntype) conv(v vm.Value, x bool) (bool, error)        { panic("TODO") }
func (intUntype) conv(v vm.Value, x bool) (*big.Int, error)     { panic("TODO") }
func (floatUntype) conv(v vm.Value, x bool) (*big.Float, error) { panic("TODO") }
func (stringUntype) conv(v vm.Value, x bool) (string, error)    { panic("TODO") }

func (t boolUntype) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	switch typ.(type) {
	case boolType, boolUntype:
		return true, nil
	}
	return false, nil
}

func (t intUntype) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	switch typ.(type) {
	case intUntype, floatUntype,
		signedType[int], signedType[int8], signedType[int16], signedType[int32], signedType[int64],
		unsignedType[uint], unsignedType[uint8], unsignedType[uint16], unsignedType[uint32], unsignedType[uint64],
		floatType[float32], floatType[float64]:
		return true, nil
	}
	return false, nil
}

func (t floatUntype) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	switch typ.(type) {
	case floatUntype,
		signedType[int], signedType[int8], signedType[int16], signedType[int32], signedType[int64],
		unsignedType[uint], unsignedType[uint8], unsignedType[uint16], unsignedType[uint32], unsignedType[uint64],
		floatType[float32], floatType[float64]:
		return true, nil
	}
	return false, nil
}

func (t stringUntype) AssignableTo(ctx vm.Context, typ vm.Type) (bool, error) {
	typ, err := common.UnderlyingType(ctx, typ)
	if err != nil {
		return false, err
	}
	switch typ.(type) {
	case stringUntype, stringType:
		return true, nil
	}
	return false, nil
}

func (t boolUntype) Convert(ctx vm.Context, v vm.Value, coerce bool) (vm.Value, error) {
	u, ok := v.(UntypedBool)
	if !ok {
		return nil, common.CannotConvert(v.Type(), t)
	}
	return u, nil
}

func (t intUntype) Convert(ctx vm.Context, v vm.Value, coerce bool) (vm.Value, error) {
	u, ok := v.(*UntypedInt)
	if !ok {
		return nil, common.CannotConvert(v.Type(), t)
	}
	return u, nil
}

func (t floatUntype) Convert(ctx vm.Context, v vm.Value, coerce bool) (vm.Value, error) {
	switch v := v.(type) {
	case *UntypedFloat:
		return v, nil
	case *UntypedInt:
		u := new(UntypedFloat)
		u.V.SetInt(&v.V)
		return u, nil
	}
	return nil, common.CannotConvert(v.Type(), t)
}

func (t stringUntype) Convert(ctx vm.Context, v vm.Value, coerce bool) (vm.Value, error) {
	u, ok := v.(UntypedString)
	if !ok {
		return nil, common.CannotConvert(v.Type(), t)
	}
	return u, nil
}

type bigVal[U comparable, T any] interface {
	vm.Value
	val() U
	typ() T
}

type bigCmpVal[U any] interface {
	~*U
	Cmp(*U) int
}

func bigOp[T primTyp[*U, V, T], V bigVal[*U, T], U any](v V, u vm.Value, op func(*U, *U, *U) *U) (V, error) {
	uu, ok := u.(V)
	if !ok {
		var z V
		return z, common.CannotConvert(v.Type(), u.Type())
	}
	return v.typ().new(op(new(U), v.val(), uu.val())), nil
}

func bigCmp[T primTyp[U, V, T], V bigVal[U, T], U bigCmpVal[W], W any](v V, u vm.Value, want int) (bool, error) {
	uu, ok := u.(V)
	if !ok {
		return false, common.CannotConvert(v.Type(), u.Type())
	}
	c := v.val().Cmp(uu.val())
	if want == 0 {
		return c == 0, nil
	}
	return c*want > 0, nil
}
