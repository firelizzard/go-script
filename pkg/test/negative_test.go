package test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/go-script/pkg/test/testlang"
)

func TestNegative(t *testing.T) {
	cases := []struct {
		Name string
		Src  string
		Err  string
	}{
		{
			Name: "Type check",
			Src: `
				package main

				// variable x is of type string
				var x int = 0
			`,
			Err: "bad value: cannot convert int to string",
		},
		{
			Name: "Within 1",
			Src: `
				package main

				// Within main, variable x is of type string
				func main() {
					var x int = 0
				}
			`,
			Err: "bad value: cannot convert int to string",
		},
		{
			Name: "Within 2",
			Src: `
				package main

				// Within main
				func main() {
					// Variable x is of type string
					var x int = 0
				}
			`,
			Err: "bad value: cannot convert int to string",
		},
		{
			Name: "Func literal",
			Src: `
				package main

				// Expression is of type int
				var _ = func() int { return 1 }
			`,
			Err: "bad value: cannot convert func() (int) to int",
		},
		{
			Name: "Func type",
			Src: `
				package main

				// Expression is a func(int) string
				var _ = func() int { return 1 }
			`,
			Err: "bad value: cannot convert func() (int) to func(int) (string)",
		},
		{
			Name: "Struct literal",
			Src: `
				package main

				type Foo struct { A, B int }

				// Expression is of type string
				var _ = Foo{1, 2}.A
			`,
			Err: "bad value: cannot convert int to string",
		},
		{
			Name: "Fails with",
			Src: `
				package main

				// Variable x fails with "foo bar"
				var x int = ""
			`,
			Err: fmt.Sprintf(
				"wrong error:\n  want: %q\n   got: %q",
				"foo bar",
				"cannot convert untyped string to int",
			),
		},
		{
			Name: "Prints",
			Src: `
				package main

				import "fmt"

				// Expression prints "foo"
				var x = func() int {
					fmt.Print("bar")
					return 1
				}()
			`,
			Err: fmt.Sprintf(
				"wrong output:\n  want: %q\n   got: %q",
				"foo",
				"bar",
			),
		},
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			err := testlang.Test([]byte(c.Src))
			require.EqualError(t, err, c.Err)
		})
	}
}
