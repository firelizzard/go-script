package main

type Foo struct{ A, B int }

// Expression is of type Foo.
var _ = Foo{1, 2}

// Expression is of type int.
var _ = Foo{1, 2}.A

// Field A of expression equals `1`.
var _ = Foo{1, 2}

// Expression `foo()` equals `3`.
func foo() int {
	x := Foo{0, 0}
	x.B = 3
	return x.B
}
