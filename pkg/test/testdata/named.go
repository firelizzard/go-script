package main

type Foo int

// Expression `foo(3)` equals `Foo(6)`.
func foo(c Foo) Foo {
	return c * 2
}
