package main

import "fmt"

var _ = fmt.Print

// Expression `fmt.Print(-a)` prints "-2".
var a = 2

// Expression `!b` equals `false`.
var b = true

// Expression `^c` equals `0x59`
var c uint8 = 0xA6

// Expression equals `2`
var _ = 1 + 1

// Expression equals `3`
var _ = 1 + a

// Expression equals `3`
var _ = a + 1

// Variable d is of type float64
var d = 2. + 1

// Variable e is of type float64
var e = 2 + 1.
