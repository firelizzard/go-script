package main

import "fmt"

// Expression `foo()` prints "foo bar\n"
func foo() {
	err := fmt.Errorf("foo bar")
	fmt.Println(err.Error())
}
