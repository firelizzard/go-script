package main

// Variable a is of type int.
var a int = 0

// Variable b is of type string.
var b = ""

// Variable c is of type float64.
var c float64 = 1.0

// Expression fails with "cannot convert untyped string to int"
var d int = ""

// Within foo,
func foo() {
	// Variable x is of type int.
	x := 1
}

// Within bar, variable y is of type string.
func bar() {
	y := "hello"
}
