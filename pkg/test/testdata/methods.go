package main

import "fmt"

type Foo struct{}
type Bar struct{}

func (Foo) Foo() Bar {
	fmt.Println("Foo")
	return Bar{}
}

func (Bar) Bar() Foo {
	fmt.Println("Bar")
	return Foo{}
}

// Expression `foo()` prints "Foo\nBar\n"
func foo() {
	var x = Foo{}
	x.Foo().Bar()
}
