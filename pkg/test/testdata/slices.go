package main

var a = [6]int{1, 2, 3, 4, 5, 6}

// Expression equals `6`
var _ = len(a)

// Expression equals `5`
var _ = a[4]

// Expression equals `[]int{3, 4, 5, 6}`
var _ = a[2:]

// Expression equals `[]int{3, 4}`
var _ = a[2:4]

// Expression equals `[]int{1, 2, 3, 4}`
var _ = a[:4]

// Expression equals `[]int{1, 2, 3, 4, 5, 6}`
var _ = a[:]

// Expression equals `[5]int{0, 0, 0, 0, 0}`
var _ = [5]int{}

// [skip] Expression `array1()` prints "51534".
func array1() {
	a := [6]int{1, 2, 3, 4, 5, 6}
	a[1] = 5
	print(a[1])
	for i, v := range a {
		print(v)
		if i == 3 {
			break
		}
	}
}
