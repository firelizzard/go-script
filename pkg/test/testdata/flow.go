package main

import "fmt"

// Expression `forThreeClause(10)` prints "0123456789".
func forThreeClause(n int) {
	for i := 0; i < n; i = i + 1 {
		fmt.Print(i)
	}
}

// Expression `ifelse(true)` prints "foo". Expression `ifelse(false)` prints
// "bar".
func ifelse(ok bool) {
	if ok {
		fmt.Print("foo")
	} else {
		fmt.Print("bar")
	}
}

// Expression `forBreak()` prints "01".
func forBreak() {
	for i := 0; i < 4; i = i + 1 {
		fmt.Print(i)
		if i > 0 {
			break
		}
	}
}

// Expression `forContinue()` prints "001123".
func forContinue() {
	for i := 0; i < 4; i = i + 1 {
		fmt.Print(i)
		if i > 1 {
			continue
		}
		fmt.Print(i)
	}
}
