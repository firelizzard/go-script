package main

// Expression `double(5)` equals `10`.
func double(v int) int { return v * 2 }

// Expression is a function.
var _ = func() {}

// Expression is a func(int) string.
var _ = func(int) string {}

// Expression is of type int.
var _ = func() int { return 1 }()
