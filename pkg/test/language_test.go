package test

import (
	"embed"
	"errors"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/firelizzard/go-script/pkg/test/testlang"
	"gitlab.com/firelizzard/go-script/pkg/vm/program"
)

//go:embed testdata/*.go
var testFiles embed.FS

var reSnake = regexp.MustCompile(`(^|_)\w`)

func TestLanguage(t *testing.T) {
	type TestCase struct {
		Name string
		Path string
		File *ast.File
	}

	wd, err := os.Getwd()
	require.NoError(t, err)

	ent, err := testFiles.ReadDir("testdata")
	require.NoError(t, err)

	fset := token.NewFileSet()
	var cases []*TestCase
	for _, ent := range ent {
		name := ent.Name()
		if strings.HasPrefix(name, "_") ||
			!strings.HasSuffix(name, ".go") {
			continue
		}

		name = strings.TrimSuffix(name, ".go")
		name = reSnake.ReplaceAllStringFunc(name, func(s string) string {
			s = strings.TrimPrefix(s, "_")
			return strings.ToUpper(s)
		})

		src, err := testFiles.ReadFile(path.Join("testdata", ent.Name()))
		require.NoError(t, err)

		fullPath := filepath.Join(wd, "testdata", ent.Name())
		f, err := parser.ParseFile(fset, fullPath, src, parser.ParseComments)
		require.NoError(t, err)

		cases = append(cases, &TestCase{name, fullPath, f})
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			err = testlang.TestFile(fset, c.File)
			var perr *program.ParseError
			var terr *testlang.TestError
			switch {
			case errors.As(err, &perr):
				t.Errorf("\n%v:\n  %v\n", fset.Position(perr.Node.Pos()), err)
			case errors.As(err, &terr):
				t.Errorf("\n%v:\n  %v\n", fset.Position(terr.Statement.Pos()), err)
			case err != nil:
				t.Errorf("\n%v:\n  %v\n", c.Path, err)
			}
		})
	}
}
