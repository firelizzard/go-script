package testlang

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"runtime/debug"
	"strings"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/parse"
	"gitlab.com/firelizzard/go-script/pkg/vm/program"
)

func Test(src []byte) error {
	// Parse Go
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "main.go", src, parser.ParseComments)
	if err != nil {
		return err
	}

	return TestFile(fset, f)
}

func TestFile(fset *token.FileSet, f *ast.File) error {
	// Parse RunScript
	world := program.NewWorld()
	scope := &testScope{
		parent:   world,
		fset:     fset,
		comments: map[int]string{},
		decls:    map[string]vm.Declaration{},
	}

	op, err := scope.parse(f)
	if err != nil {
		return err
	}

	// Execute
	ctx := world.NewContext()
	ctx.Scope().Push(scope)
	return op.Execute(ctx)
}

func (t *testScope) parse(src ast.Node) (vm.Op, error) {
	switch src := src.(type) {
	case *ast.File:
		for _, c := range src.Comments {
			pos := t.fset.Position(c.End())
			t.comments[pos.Line+1] = c.Text()
		}

		var all execute.Program
		for _, decl := range src.Decls {
			op, err := t.parse(decl)
			if err != nil {
				return nil, err
			}
			all = append(all, op)
		}
		return all, nil

	case *ast.GenDecl:
		return parseTestOrSkip(src, src.Doc)

	case *ast.FuncDecl:
		return parseTestOrSkip(src, src.Doc)

	default:
		pos := t.fset.Position(src.Pos())
		text, ok := t.comments[pos.Line]
		if !ok {
			val, err := parse.Parse(parse.Node, src)
			if err != nil {
				return nil, err
			}
			return execute.Program(val), nil
		}

		return parseTestOrSkip2(src, text)
	}
}

func parseTestOrSkip(src ast.Node, doc *ast.CommentGroup) (execute.Program, error) {
	if doc == nil {
		stmt, err := parse.Parse(parse.Node, src)
		if err != nil {
			return nil, err
		}
		return execute.Program(stmt), nil
	}

	text := doc.Text()
	if strings.HasPrefix(text, "[skip]") {
		// TODO: Notify that we're skipping?
		return execute.Program{}, nil
	}

	return parseTestOrSkip2(src, text)
}

func parseTestOrSkip2(src ast.Node, text string) (execute.Program, error) {
	expr, err := Parse("test", text)
	if err != nil {
		return nil, err
	}

	return execute.Program{
		&srcStmt{src},
		&testStmt{expr},
	}, nil
}

type testStmt struct {
	test Expression
}

func (s *testStmt) Type() vm.Type { panic("") }

func (s *testStmt) Execute(ctx vm.Context) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = &panicked{
				Err: r,
				Msg: fmt.Sprintf("panicked while executing statement\n\t%+v\n\t%v\n\n%s", s.test, r, debug.Stack()),
			}
		}
	}()

	var src ast.Node
	if ctx.Stack().Len() > 0 {
		stmt, ok := ctx.Stack().Peek().(*srcStmt)
		if ok {
			src = stmt.src
		}
	}

	stmt, err := Compile(s.test)
	if err != nil {
		return err
	}

	err = stmt.Execute(ctx)
	if err != nil && src != nil {
		return &TestError{Statement: src, Wrapped: err}
	}
	return err
}

type panicked struct {
	Err any
	Msg string
}

func (p *panicked) Error() string { return p.Msg }

type srcStmt struct {
	src ast.Node
}

func (s *srcStmt) Type() vm.Type { panic("") }

type testScope struct {
	parent   vm.Scope
	decls    map[string]vm.Declaration
	fset     *token.FileSet
	comments map[int]string
}

func (s *testScope) Resolve(ctx vm.Context, name string) (vm.Declaration, error) {
	if d, ok := s.decls[name]; ok {
		return d, nil
	}
	return s.parent.Resolve(ctx, name)
}

func (s *testScope) Declare(_ vm.Context, decl vm.Declaration) error {
	name := decl.Name()
	if _, ok := s.decls[name]; ok {
		return fmt.Errorf("%v has already been declared", name)
	}
	s.decls[name] = decl
	return nil
}
