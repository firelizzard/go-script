package testlang

import (
	"bytes"
	"errors"
	"fmt"
	"go/ast"
	"go/token"
	"reflect"

	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/common"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/inject"
	"gitlab.com/firelizzard/go-script/pkg/vm/operations"
	"gitlab.com/firelizzard/go-script/pkg/vm/parse"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

type Statement interface {
	Execute(ctx vm.Context) error
}

type nopStmt struct{}

func (s nopStmt) Execute(vm.Context) error { return nil }

type multiStmt []Statement

func (s multiStmt) flatten() multiStmt {
	var t multiStmt
	for _, s := range s {
		switch s := s.(type) {
		case multiStmt:
			t = append(t, s.flatten()...)
		default:
			t = append(t, s)
		}
	}
	return t
}

func (s multiStmt) append(t ...Statement) multiStmt {
	for _, t := range t {
		switch t := t.(type) {
		case multiStmt:
			s = s.append(t...)
		default:
			s = append(s, t)
		}
	}
	return s
}

func (s multiStmt) copy() Statement {
	switch len(s) {
	case 0:
		return s
	case 1:
		return s[0]
	}

	t := make(multiStmt, len(s))
	copy(t, s)
	return t
}

func (s multiStmt) Execute(ctx vm.Context) error {
	for _, s := range s {
		err := s.Execute(ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

type badStmt struct{}

func (s badStmt) Execute(ctx vm.Context) error {
	return errors.New("invalid statement")
}

type exprIs[N ast.Node] struct{}

func (exprIs[N]) Execute(ctx vm.Context) error {
	v := ctx.Stack().Peek()
	src, ok := v.(*srcStmt)
	if !ok {
		return fmt.Errorf("expected %T, got %T", (*srcStmt)(nil), v)
	}
	_, ok = src.src.(N)
	if !ok {
		var z N
		return fmt.Errorf("expected %T, got %T", z, v)
	}
	return nil
}

type execSrc struct{}

func (execSrc) Execute(ctx vm.Context) error {
	// If there's source on the stack, execute it
	if ctx.Stack().Len() == 0 {
		return nil
	}
	src, ok := ctx.Stack().Peek().(*srcStmt)
	if !ok {
		return nil
	}
	ctx.Stack().Pop()

	stmt, err := parse.Parse(parse.Node, src.src)
	if err != nil {
		return err
	}
	return execute.Program(stmt).Execute(ctx)
}

type execFunc struct {
	inner Expression
}

func (x execFunc) Execute(ctx vm.Context) error {
	src := ctx.Stack().Pop().(*srcStmt).src.(*ast.FuncDecl)

	s, _ := ctx.Scope().Find(func(s vm.Scope) bool {
		_, ok := s.(*testScope)
		return ok
	})
	ts, _ := s.(*testScope)

	var body execute.Program
	for _, src := range src.Body.List {
		op, err := ts.parse(src)
		if err != nil {
			return err
		}
		body = append(body, op)
	}
	if x.inner != nil {
		body = append(body, &testStmt{test: x.inner})
	}

	typ, err := parse.Parse[ast.Expr](parse.Expr, src.Type)
	if err != nil {
		return err
	}

	return execute.Program{
		body.AsLiteral(),
		typ,
		values.String{V: src.Name.Name},
		&values.OpMakeFuncDecl{},
		&execute.Declare{},
	}.Execute(ctx)
}

type execExpr struct{}

func (s execExpr) Execute(ctx vm.Context) error {
	// Verify the source is `var <name> = <expr>`
	src := ctx.Stack().Pop().(*srcStmt).src
	if decl, ok := src.(*ast.GenDecl); !ok {
		return fmt.Errorf("not an expression: want %T, got %T", (*ast.GenDecl)(nil), src)
	} else if decl.Tok != token.VAR {
		return fmt.Errorf("not an expression: want %T, got %T", token.VAR, decl.Tok)
	} else if len(decl.Specs) != 1 {
		return fmt.Errorf("not an expression: want one expression, got %d", len(decl.Specs))
	} else {
		src = decl.Specs[0]
	}
	if spec, ok := src.(*ast.ValueSpec); !ok {
		return fmt.Errorf("not an expression: want %T, got %T", (*ast.ValueSpec)(nil), src)
	} else if len(spec.Names) != 1 {
		return fmt.Errorf("not an expression: want one name, got %d", len(spec.Names))
	} else if len(spec.Values) != 1 {
		return fmt.Errorf("not an expression: want one value, got %d", len(spec.Values))
	} else if spec.Names[0].Name == "_" {
		src = spec.Values[0]
	}

	stmt, err := parse.Parse(parse.Node, src)
	if err != nil {
		return err
	}
	return execute.Program(stmt).Execute(ctx)
}

type resolve string

func (r resolve) Execute(ctx vm.Context) error {
	decl, err := ctx.Scope().Peek().Resolve(ctx, string(r))
	if err != nil {
		return err
	}
	ctx.Stack().Push(decl)
	return nil
}

type call struct{}

func (call) Execute(ctx vm.Context) error {
	fn, err := pop[vm.Func](ctx)
	if err != nil {
		return err
	}

	// Assume zero args
	out, err := fn.Call(ctx, []vm.Value{})
	if err != nil {
		return err
	}

	ctx.Stack().Push(out...)
	return nil
}

type declIs vm.DeclKind

func (d declIs) Execute(ctx vm.Context) error {
	kind := vm.DeclKind(d)
	decl := ctx.Stack().Peek().(vm.Declaration)
	if decl.Kind() != kind {
		return fmt.Errorf("bad declaration: want %v, got %v", kind, decl.Kind())
	}
	return nil
}

type valueIs[T vm.Value] struct{}

func (valueIs[T]) Execute(ctx vm.Context) error {
	v, err := common.Unwrap[vm.Value](ctx, ctx.Stack().Peek())
	if err != nil {
		return err
	}
	_, ok := v.(T)
	if !ok {
		return fmt.Errorf("bad value: want %v, got %T", reflect.TypeFor[T](), ctx.Stack().Peek())
	}
	return nil
}

type valueIsOfType struct{}

func (valueIsOfType) Execute(ctx vm.Context) error {
	typ, err := pop[vm.Type](ctx)
	if err != nil {
		return err
	}
	val, err := pop[vm.Value](ctx)
	if err != nil {
		return err
	}
	_, err = typ.Convert(ctx, val, false)
	if err != nil {
		return fmt.Errorf("bad value: %w", err)
	}
	return nil
}

type funcSigIs struct{}

func (f funcSigIs) Execute(ctx vm.Context) error {
	args := ctx.Stack().PopN(2)
	val, typ := args[0], args[1].(vm.Type)
	_, err := typ.Convert(ctx, val, false)
	if err != nil {
		return fmt.Errorf("bad value: %w", err)
	}
	return nil
}

type valuesEqual struct{}

var opEqual = operations.Binary(func(v operations.EqualValue, u vm.Value) (vm.Value, error) {
	ok, err := v.Equal(u)
	return values.Bool{V: ok}, err
}).Named("==").SameIn()

func (valuesEqual) Execute(ctx vm.Context) error {
	var err error
	v := ctx.Stack().PopN(2)
	v[0], err = common.Unwrap[vm.Value](ctx, v[0])
	if err != nil {
		return err
	}
	v[1], err = common.Unwrap[vm.Value](ctx, v[1])
	if err != nil {
		return err
	}

	_, ok := common.Underlying(v[0]).(operations.EqualValue)
	if ok {
		w, err := opEqual.Apply(ctx, v[0], v[1])
		if err != nil {
			return err
		}
		ok = w.(values.Bool).AsBool()
	} else {
		ok = reflect.DeepEqual(v[0], v[1])
	}
	if !ok {
		return fmt.Errorf("bad value: %v != %v", v[0], v[1])
	}
	return nil
}

type member string

func (s member) Execute(ctx vm.Context) error {
	v, err := pop[vm.Resolver](ctx)
	if err != nil {
		return err
	}
	decl, err := v.Resolve(ctx, string(s))
	if err != nil {
		return err
	}
	ctx.Stack().Push(decl)
	return nil
}

type failStmt struct {
	inner Statement
	cond  func(error) error
}

func (f *failStmt) Execute(ctx vm.Context) error {
	err := f.inner.Execute(ctx)
	if err == nil {
		return errors.New("expected statement to fail")
	}
	return f.cond(err)
}

type stdoutStmt struct {
	inner Statement
	cond  func([]byte) error
}

func (f *stdoutStmt) Execute(ctx vm.Context) error {
	buf := new(bytes.Buffer)
	ctx = inject.CaptureStdout(ctx, buf)

	err := f.inner.Execute(ctx)
	if err != nil {
		return err
	}

	return f.cond(buf.Bytes())
}

func pop[V any](ctx vm.Context) (V, error) {
	var z V
	v, err := common.PopUnwrap[vm.Value](ctx)
	if err != nil {
		return z, err
	}
	u, ok := v.(V)
	if !ok {
		return z, fmt.Errorf("bad value: want %v, got %T", reflect.TypeFor[V](), v)
	}
	return u, nil
}
