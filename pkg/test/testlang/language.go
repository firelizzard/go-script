package testlang

import (
	"fmt"
	"go/ast"
	"go/parser"
	"strconv"

	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer"
	"gitlab.com/firelizzard/go-script/pkg/vm"
	"gitlab.com/firelizzard/go-script/pkg/vm/execute"
	"gitlab.com/firelizzard/go-script/pkg/vm/parse"
	"gitlab.com/firelizzard/go-script/pkg/vm/values"
)

func Parse(filename, src string) (Expression, error) {
	x, err := defaultParser.ParseString(filename, src)
	if err != nil {
		return nil, err
	}
	if len(x.Expressions) == 1 {
		return x.Expressions[0], nil
	}
	return x.Expressions, nil
}

func Compile(expr Expression) (Statement, error) {
	stmt, err := expr.compile()
	if err != nil {
		return nil, err
	}

	// Simplify
	m, ok := stmt.(multiStmt)
	if !ok {
		return stmt, nil
	}
	m = m.flatten()
	if len(m) == 1 {
		return m[0], nil
	}
	return m, nil
}

type expressionSet struct {
	Expressions Expressions `@@ (("." | ",") @@)* ("." | ",")?`
}

var defaultParser = func() *participle.Parser[expressionSet] {
	parser, err := newParser()
	if err != nil {
		panic(err)
	}
	return parser
}()

func newParser() (*participle.Parser[expressionSet], error) {
	return participle.Build[expressionSet](
		participle.CaseInsensitive("Ident"),
		participle.UseLookahead(participle.MaxLookahead),
		participle.Unquote("String", "RawString"),

		participle.Union[Expression](
			&Within{}, &FailsWith{}, &Prints{},
			&IsFunction{}, &IsOfType{}, &IsEqualTo{},
		),
		participle.Union[Value](
			&Variable{}, &Field{}, &ExprValue{}, &FuncSig{},
			&Ident{}, &Int{}, &Float{}, &String{}, &RawString{},
		),
	)
}

type langStmt interface {
	compile() (Statement, error)
}

type Expression interface {
	expr()
	langStmt
}

type Value interface {
	value()
	langStmt
}

type Expressions []Expression

func (Expressions) expr() {}

func (e Expressions) compile() (Statement, error) {
	var all multiStmt
	for _, expr := range e {
		stmt, err := Compile(expr)
		if err != nil {
			return nil, err
		}
		all = all.append(stmt)
	}
	return all, nil
}

type Within struct {
	Pos   lexer.Position
	Name  string      `"Within" @Ident`
	Inner Expressions `( ","? @@ ((";" | ",") "and"? @@)* )?`
}

func (*Within) expr() {}

func (e *Within) compile() (Statement, error) {
	return multiStmt{
		exprIs[*ast.FuncDecl]{},
		execFunc{inner: e.Inner},
		resolve(e.Name),
		call{},
	}, nil
}

type IsEqualTo struct {
	Got  Value `@@`
	Want Value `("is"? "equal" "to" | "equals") @@`
}

func (*IsEqualTo) expr() {}

func (c *IsEqualTo) compile() (Statement, error) {
	got, err := c.Got.compile()
	if err != nil {
		return nil, err
	}
	want, err := c.Want.compile()
	if err != nil {
		return nil, err
	}
	return multiStmt{
		got,
		want,
		valuesEqual{},
	}, nil
}

type IsOfType struct {
	Pos   lexer.Position
	Value Value `@@`
	Type  Value `("is"? "of" "type" | "is" ("a" | "an")) @@`
}

func (*IsOfType) expr() {}

func (c *IsOfType) compile() (Statement, error) {
	value, err := c.Value.compile()
	if err != nil {
		return nil, err
	}
	typ, err := c.Type.compile()
	if err != nil {
		return nil, err
	}
	return multiStmt{
		value,
		typ,
		valueIs[vm.Type]{},
		valueIsOfType{},
	}, nil
}

type IsFunction struct {
	Pos   lexer.Position
	Value Value `@@ "is" "a"? "function"`
}

func (*IsFunction) expr() {}

func (c *IsFunction) compile() (Statement, error) {
	value, err := c.Value.compile()
	if err != nil {
		return nil, err
	}
	return multiStmt{
		value,
		valueIs[vm.Func]{},
	}, nil
}

type FailsWith struct {
	Pos   lexer.Position
	Value Value  `@@`
	Error string `"fails" "with" (@String | @RawString)`
}

func (*FailsWith) expr() {}

func (c *FailsWith) compile() (Statement, error) {
	value, err := c.Value.compile()
	if err != nil {
		return nil, err
	}
	return &failStmt{
		inner: value,
		cond: func(err error) error {
			if err.Error() != c.Error {
				return fmt.Errorf("wrong error:\n  want: %q\n   got: %q", c.Error, err.Error())
			}
			return nil
		},
	}, nil
}

type Prints struct {
	Pos    lexer.Position
	Value  Value  `@@`
	Output string `"prints" (@String | @RawString)`
}

func (*Prints) expr() {}

func (c *Prints) compile() (Statement, error) {
	value, err := c.Value.compile()
	if err != nil {
		return nil, err
	}
	return &stdoutStmt{
		inner: value,
		cond: func(out []byte) error {
			if string(out) != c.Output {
				return fmt.Errorf("wrong output:\n  want: %q\n   got: %q", c.Output, out)
			}
			return nil
		},
	}, nil
}

type Variable struct {
	Pos  lexer.Position
	Name string `"variable" @Ident`
}

func (*Variable) value() {}

func (e *Variable) compile() (Statement, error) {
	return multiStmt{
		execSrc{},
		resolve(e.Name),
		declIs(vm.DeclVar),
	}, nil
}

type Field struct {
	Pos   lexer.Position
	Name  string `"field" @Ident`
	Value Value  `"of" @@`
}

func (*Field) value() {}

func (e *Field) compile() (Statement, error) {
	value, err := e.Value.compile()
	if err != nil {
		return nil, err
	}
	return multiStmt{
		value,
		member(e.Name),
	}, nil
}

type ExprValue struct {
	Pos   lexer.Position
	Value *RawString `'expression' @@?`
}

func (*ExprValue) value() {}

func (e *ExprValue) compile() (Statement, error) {
	if e.Value == nil {
		return multiStmt{
			execExpr{},
		}, nil
	}

	value, err := e.Value.compile()
	if err != nil {
		return nil, err
	}
	return multiStmt{
		execSrc{},
		value,
	}, nil
}

type FuncSig struct {
	In  []*Ident `"func" "(" (@@ ("," @@)*)? ")"`
	Out []*Ident `(@@ | "(" (@@ ("," @@)*)? ")" )`
}

func (f *FuncSig) value() {}

func (f *FuncSig) compile() (Statement, error) {
	var stmt multiStmt
	for _, types := range [][]*Ident{f.In, f.Out} {
		for _, typ := range types {
			stmt = stmt.append(
				resolve(typ.Value),
				valueIs[vm.Type]{},
			)
		}
	}
	stmt = stmt.append(
		&values.OpMakeFuncSig{In: len(f.In), Out: len(f.Out)},
	)
	return stmt, nil
}

type Ident struct {
	Value string `@Ident`
}

func (*Ident) value() {}

func (v *Ident) compile() (Statement, error) {
	return resolve(v.Value), nil
}

type Int struct {
	Value string `@Int`
}

func (*Int) value() {}

func (v *Int) compile() (Statement, error) {
	u, err := strconv.ParseInt(v.Value, 10, 64)
	if err != nil {
		return nil, err
	}
	return execute.Program{values.Int64{V: u}}, nil
}

type Float struct {
	Value string `@Float`
}

func (*Float) value() {}

func (v *Float) compile() (Statement, error) {
	u, err := strconv.ParseFloat(v.Value, 64)
	if err != nil {
		return nil, err
	}
	return execute.Program{values.Float64{V: u}}, nil
}

type String struct {
	Value string `@String`
}

func (*String) value() {}

func (v *String) compile() (Statement, error) {
	return execute.Program{values.String{V: v.Value}}, nil
}

type RawString struct {
	Value string `@RawString`
}

func (*RawString) value() {}

func (v *RawString) compile() (Statement, error) {
	src, err := parser.ParseExpr(v.Value)
	if err != nil {
		return nil, err
	}
	return parse.Parse(parse.Expr, src)
}
