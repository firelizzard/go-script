package testlang

import "go/ast"

type TestError struct {
	Statement ast.Node
	Wrapped   error
}

func (e *TestError) Error() string {
	return e.Wrapped.Error()
}

func (e *TestError) Unwrap() error {
	return e.Wrapped
}
